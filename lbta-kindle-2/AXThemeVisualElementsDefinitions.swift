
import UIKit
//  example usage:
//        let defs = VisualElementDefinitions.create(from: "ax_theme_1")
//        let d = defs?.generateDictOfColorsFontsGradients()

struct AxThemeVisualElementDefinitions: Codable {
    // createsa the VED struct which has arrays for color/font/gradient numbers NOT INSTANCES
    static func create(from fileName: String) -> AxThemeVisualElementDefinitions? {
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "json") else {
            fatalError("Can not find \(fileName).json  in main bundle!")
        }
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode( AxThemeVisualElementDefinitions.self, from: data)
            return jsonData
        } catch {
            fatalError("JSON parse error:\(error)")
        }
    }
    let colors: [String: [Int] ]  //   [r, g, b, a, r2, g2, b2, a2, ...]
    let gradients: [String: GradientModel]
    let fonts: [String: FontModel]
    // Instantiates the colors/fonts/gradients and puts them in a dictionary
    func generateDictOfColorsFontsGradients() -> [String: Any] {
        var d = [String:Any]()
        // Keys of dictionary:
        // <element>Color
        // <element>Font
        // <elememnt>Gradient
        
        for eachColor in colors {
            let newKey = eachColor.key + "Color"
            print("converting" + newKey)
            let r = CGFloat(eachColor.value[0])
            let g = CGFloat(eachColor.value[1])
            let b = CGFloat(eachColor.value[2])
            let a = CGFloat(eachColor.value[3])
            d[newKey] = UIColor(red: r, green: g, blue: b, alpha: a)
        }
        for eachGradient in gradients {
            let newKey = eachGradient.key + "Gradient"
            print("converting" + newKey)
            print(eachGradient.value.colors)
            print(eachGradient.value.locations)
            print(eachGradient.value.startPoint)
            print(eachGradient.value.endPoint)
            print("=========================")
            
            let g = CAGradientLayer()
            var colors = [Any]()
            for ix in stride(from: 0, to: eachGradient.value.colors.count, by: 4) {
                let red = eachGradient.value.colors[ix]
                let green = eachGradient.value.colors[ix+1]
                let blue = eachGradient.value.colors[ix+2]
                let alpha = eachGradient.value.colors[ix+3]
                print(red, green, blue, alpha)
                let aColor = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: CGFloat(alpha )).cgColor
                colors.append(aColor)
            }
            g.colors = colors
            
            var locations = [NSNumber]()
            for eachNum in eachGradient.value.locations {
                locations.append(NSNumber(value: eachNum))
            }
            g.locations = locations
            
            let x1 = eachGradient.value.startPoint[0]
            let y1 = eachGradient.value.startPoint[1]
            g.startPoint = CGPoint(x: CGFloat(x1), y: CGFloat(y1))
            let x2 = eachGradient.value.endPoint[0]
            let y2 = eachGradient.value.endPoint[1]
            g.endPoint = CGPoint(x: CGFloat(x2), y: CGFloat(y2))
            d[newKey] = g
        }
        //        https://github.com/lionhylra/iOS-UIFont-Names
        for eachFont in fonts {
            let newKey = eachFont.key + "Font"
            let family = eachFont.value.family
            let face = eachFont.value.face
            let size  = eachFont.value.size
            // print(newKey)
            //print(eachFont.value.family, eachFont.value.face, eachFont.value.size)
            var font:UIFont
            if family == nil {
                // use system font if family not specified
                switch face  {
                case nil:
                    font = UIFont.systemFont(ofSize: CGFloat(size))
                case "bold":
                    font = UIFont.boldSystemFont(ofSize: CGFloat(size))
                case "italic":
                    font = UIFont.italicSystemFont(ofSize: CGFloat(size))
                default:
                    font = UIFont.systemFont(ofSize: CGFloat(17))
                }
            } else {
                if eachFont.value.face == nil {
                    // A family may not have a face, so don't include the dangling dash
                    font = UIFont(name: eachFont.value.family!,
                                  size: CGFloat(eachFont.value.size)) ?? UIFont.systemFont(ofSize: 17)
                } else {
                    font = UIFont(name: eachFont.value.family! + "-" + eachFont.value.face!,
                                  size: CGFloat(eachFont.value.size)) ?? UIFont.systemFont(ofSize: 17)
                }
            }
            d[newKey] = font
        }
        return d
    }
    struct FontModel: Codable {
        let family: String?
        let face: String?
        let size: CGFloat
    }
    struct GradientModel: Codable {
        let colors:[Double]
        let locations:[Double]
        let startPoint: [Double]  // x and y
        let endPoint: [Double] // x and y
        enum CodingKeys: String, CodingKey {
            case colors  = "colors" // make an array of ints called colors, 4-tuple for each color
            case locations = "locations"
            case startPoint = "start_point"
            case endPoint = "end_point"
        }
    }
}

