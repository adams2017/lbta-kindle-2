//
//  CustomCell.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class CustomCell:UITableViewCell {
   
    var bookForCell:Book? {
        didSet {
            guard let bookForCell = bookForCell else {
                fatalError()
            }
            self.titleLabel.text = bookForCell.title
            self.authorLabel.text = bookForCell.author
            
            var image = UIImage()
            
            guard let imageURL = URL(string: bookForCell.coverImageUrl) else {
                fatalError()
            }
            let task = URLSession.shared.dataTask(with: imageURL) { (data, response, error) in
                
                if error != nil {
                    print("There is an error ", error ?? "" )
                }
                guard let data = data else {
                    fatalError("no data")
                }
                
                image = UIImage(data: data ) ?? UIImage()
                
                DispatchQueue.main.async {
                    self.coverImageView.image = image
                }
            }
            task.resume()

        }
    }
    
    let  titleLabel:UILabel = {
        let ui = UILabel()
        ui.text = "title"
        ui.font = UIFont.systemFont(ofSize: 16)
        ui.textColor = axTheme.color(of: .labelText)
        ui.backgroundColor = axTheme.color(of: .labelBackground)
        //ui.backgroundColor
        ui.numberOfLines = 1
        return ui
    }()
    
      let authorLabel:UILabel = {
        let ui = UILabel()
        ui.text = "author"
        ui.font = UIFont.systemFont(ofSize: 16)
        ui.textColor = axTheme.color(of: .labelText)
        ui.backgroundColor = axTheme.color(of: .labelBackground)
        ui.numberOfLines = 1
        return ui
    }()
    private let coverImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = UIImage()
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        return ui
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        setupConstraints()        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setup() {
        self.backgroundColor = .clear
        
        
    }
    
    func setupConstraints() {
        
        [coverImageView, titleLabel, authorLabel].forEach {
            self.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints=false}
        
        NSLayoutConstraint.activate([
            coverImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            coverImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            coverImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
            coverImageView.widthAnchor.constraint(equalToConstant: 50),
            
            titleLabel.topAnchor.constraint(equalTo: coverImageView.topAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 20),
            titleLabel.leftAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: 8),
            titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            
            authorLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            authorLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            authorLabel.heightAnchor.constraint(equalTo: titleLabel.heightAnchor),
            authorLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor)
            
            ])
    }
    
    
}
