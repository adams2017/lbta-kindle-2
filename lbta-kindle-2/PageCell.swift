//
//  PageCell.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class PageCell: UICollectionViewCell {
    //Mark: PROPERTIES
    let textLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ui.font = UIFont.systemFont(ofSize: 18)
        ui.textColor = UIColor.black
        ui.backgroundColor = UIColor.white
        ui.numberOfLines = 0
        
        return ui
    }()

    
    static var reuseIdentifier: String {
        return self.description()
    }
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        
       // textLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       // self.addSubview(textLabel)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var aPage:Page? {
        didSet {
            guard let aPage = aPage else {return}
        }
        
    }
    
    func setup() {
        self.backgroundColor = .white
        [ textLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        
        NSLayoutConstraint.activate([
            //textLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            //textLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            textLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            textLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
            textLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            textLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20)
            
            ])
    }
    //MARK: UI Elements =======================================================
    //MARK:
}

