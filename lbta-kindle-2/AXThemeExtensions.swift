//
//  AXThemeExtensions.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/27/19.
//  Copyright © 2019 AX Software. All rights reserved.
//
//
//  Extensions.swift
//  test-axThemeKit
//
//  Created by Admin on 8/10/19.
//  Copyright © 2019 AX Software. All rights reserved.
//


//https://devblog.xero.com/managing-ui-colours-with-ios-11-asset-catalogs-16500ba48205

import UIKit

extension UIView { //snip zxcagrad
    func setGradientBackground(color1: UIColor = .white, color2: UIColor = .darkGray) {
        let gl = CAGradientLayer()
        gl.frame = bounds
        gl.colors = [color1.cgColor, color2.cgColor ]
        gl.locations = [0.0, 1.0]
        gl.startPoint = CGPoint(x: 1, y: 1.0)
        gl.endPoint = CGPoint(x: 0, y: 0)
        layer.insertSublayer(gl, at: 0)
    }
}

extension UIColor {
    // from // https://cloford.com/resources/colours/500col.htm
    static let axIndianred = UIColor(red:176/255,green:23/255,blue:31/255,alpha:1)
    static let axCimson = UIColor(red:220/255,green:20/255,blue:60/255,alpha:1)
    static let axLightpink = UIColor(red:255/255,green:182/255,blue:193/255,alpha:1)
    static let axLightpink1 = UIColor(red:255/255,green:174/255,blue:185/255,alpha:1)
    static let axLightpink2 = UIColor(red:238/255,green:162/255,blue:173/255,alpha:1)
    static let axLightpink3 = UIColor(red:205/255,green:140/255,blue:149/255,alpha:1)
    static let axLightpink4 = UIColor(red:139/255,green:95/255,blue:101/255,alpha:1)
    static let axPink = UIColor(red:255/255,green:192/255,blue:203/255,alpha:1)
    static let axPink1 = UIColor(red:255/255,green:181/255,blue:197/255,alpha:1)
    static let axPink2 = UIColor(red:238/255,green:169/255,blue:184/255,alpha:1)
    static let axPink3 = UIColor(red:205/255,green:145/255,blue:158/255,alpha:1)
    static let axPink4 = UIColor(red:139/255,green:99/255,blue:108/255,alpha:1)
    static let axPalevioletred = UIColor(red:219/255,green:112/255,blue:147/255,alpha:1)
    static let axPalevioletred1 = UIColor(red:255/255,green:130/255,blue:171/255,alpha:1)
    static let axPalevioletred2 = UIColor(red:238/255,green:121/255,blue:159/255,alpha:1)
    static let axPalevioletred3 = UIColor(red:205/255,green:104/255,blue:137/255,alpha:1)
    static let axPalevioletred4 = UIColor(red:139/255,green:71/255,blue:93/255,alpha:1)
    static let axLavenderblush1 = UIColor(red:255/255,green:240/255,blue:245/255,alpha:1)
    static let axLavenderblush2 = UIColor(red:238/255,green:224/255,blue:229/255,alpha:1)
    static let axLavenderblush3 = UIColor(red:205/255,green:193/255,blue:197/255,alpha:1)
    static let axLavenderblush4 = UIColor(red:139/255,green:131/255,blue:134/255,alpha:1)
    static let axVioletred1 = UIColor(red:255/255,green:62/255,blue:150/255,alpha:1)
    static let axVioletred2 = UIColor(red:238/255,green:58/255,blue:140/255,alpha:1)
    static let axVioletred3 = UIColor(red:205/255,green:50/255,blue:120/255,alpha:1)
    static let axVioletred4 = UIColor(red:139/255,green:34/255,blue:82/255,alpha:1)
    static let axHotpink = UIColor(red:255/255,green:105/255,blue:180/255,alpha:1)
    static let axHotpink1 = UIColor(red:255/255,green:110/255,blue:180/255,alpha:1)
    static let axHotpink2 = UIColor(red:238/255,green:106/255,blue:167/255,alpha:1)
    static let axHotpink3 = UIColor(red:205/255,green:96/255,blue:144/255,alpha:1)
    static let axHotpink4 = UIColor(red:139/255,green:58/255,blue:98/255,alpha:1)
    static let axRaspberry = UIColor(red:135/255,green:38/255,blue:87/255,alpha:1)
    static let axDeeppink = UIColor(red:255/255,green:20/255,blue:147/255,alpha:1)
    static let axDeeppink2 = UIColor(red:238/255,green:18/255,blue:137/255,alpha:1)
    static let axDeeppink3 = UIColor(red:205/255,green:16/255,blue:118/255,alpha:1)
    static let axDeeppink4 = UIColor(red:139/255,green:10/255,blue:80/255,alpha:1)
    static let axMaroon1 = UIColor(red:255/255,green:52/255,blue:179/255,alpha:1)
    static let axMaroon2 = UIColor(red:238/255,green:48/255,blue:167/255,alpha:1)
    static let axMaroon3 = UIColor(red:205/255,green:41/255,blue:144/255,alpha:1)
    static let axMaroon4 = UIColor(red:139/255,green:28/255,blue:98/255,alpha:1)
    static let axMediumvioletred = UIColor(red:199/255,green:21/255,blue:133/255,alpha:1)
    static let axVioletred = UIColor(red:208/255,green:32/255,blue:144/255,alpha:1)
    static let axOrchid = UIColor(red:218/255,green:112/255,blue:214/255,alpha:1)
    static let axOrchid1 = UIColor(red:255/255,green:131/255,blue:250/255,alpha:1)
    static let axOrchid2 = UIColor(red:238/255,green:122/255,blue:233/255,alpha:1)
    static let axOrchid3 = UIColor(red:205/255,green:105/255,blue:201/255,alpha:1)
    static let axOrchid4 = UIColor(red:139/255,green:71/255,blue:137/255,alpha:1)
    static let axThistle = UIColor(red:216/255,green:191/255,blue:216/255,alpha:1)
    static let axThistle1 = UIColor(red:255/255,green:225/255,blue:255/255,alpha:1)
    static let axThistle2 = UIColor(red:238/255,green:210/255,blue:238/255,alpha:1)
    static let axThistle3 = UIColor(red:205/255,green:181/255,blue:205/255,alpha:1)
    static let axThistle4 = UIColor(red:139/255,green:123/255,blue:139/255,alpha:1)
    static let axPlum1 = UIColor(red:255/255,green:187/255,blue:255/255,alpha:1)
    static let axPlum2 = UIColor(red:238/255,green:174/255,blue:238/255,alpha:1)
    static let axPlum3 = UIColor(red:205/255,green:150/255,blue:205/255,alpha:1)
    static let axPlum4 = UIColor(red:139/255,green:102/255,blue:139/255,alpha:1)
    static let axPlum = UIColor(red:221/255,green:160/255,blue:221/255,alpha:1)
    static let axViolet = UIColor(red:238/255,green:130/255,blue:238/255,alpha:1)
    static let axMagenta = UIColor(red:255/255,green:0/255,blue:255/255,alpha:1)
    static let axMagenta2 = UIColor(red:238/255,green:0/255,blue:238/255,alpha:1)
    static let axMagenta3 = UIColor(red:205/255,green:0/255,blue:205/255,alpha:1)
    static let axMagenta4 = UIColor(red:139/255,green:0/255,blue:139/255,alpha:1)
    static let axPurple = UIColor(red:128/255,green:0/255,blue:128/255,alpha:1)
    static let axMediumorchid = UIColor(red:186/255,green:85/255,blue:211/255,alpha:1)
    static let axMediumorchid1 = UIColor(red:224/255,green:102/255,blue:255/255,alpha:1)
    static let axMediumorchid2 = UIColor(red:209/255,green:95/255,blue:238/255,alpha:1)
    static let axMediumorchid3 = UIColor(red:180/255,green:82/255,blue:205/255,alpha:1)
    static let axMediumorchid4 = UIColor(red:122/255,green:55/255,blue:139/255,alpha:1)
    static let axDarkviolet = UIColor(red:148/255,green:0/255,blue:211/255,alpha:1)
    static let axDarkorchid = UIColor(red:153/255,green:50/255,blue:204/255,alpha:1)
    static let axDarkorchid1 = UIColor(red:191/255,green:62/255,blue:255/255,alpha:1)
    static let axDarkorchid2 = UIColor(red:178/255,green:58/255,blue:238/255,alpha:1)
    static let axDarkorchid3 = UIColor(red:154/255,green:50/255,blue:205/255,alpha:1)
    static let axDarkorchid4 = UIColor(red:104/255,green:34/255,blue:139/255,alpha:1)
    static let axIndigo = UIColor(red:75/255,green:0/255,blue:130/255,alpha:1)
    static let axBlueviolet = UIColor(red:138/255,green:43/255,blue:226/255,alpha:1)
    static let axPurple1 = UIColor(red:155/255,green:48/255,blue:255/255,alpha:1)
    static let axPurple2 = UIColor(red:145/255,green:44/255,blue:238/255,alpha:1)
    static let axPurple3 = UIColor(red:125/255,green:38/255,blue:205/255,alpha:1)
    static let axPurple4 = UIColor(red:85/255,green:26/255,blue:139/255,alpha:1)
    static let axMediumpurple = UIColor(red:147/255,green:112/255,blue:219/255,alpha:1)
    static let axMediumpurple1 = UIColor(red:171/255,green:130/255,blue:255/255,alpha:1)
    static let axMediumpurple2 = UIColor(red:159/255,green:121/255,blue:238/255,alpha:1)
    static let axMediumpurple3 = UIColor(red:137/255,green:104/255,blue:205/255,alpha:1)
    static let axMediumpurple4 = UIColor(red:93/255,green:71/255,blue:139/255,alpha:1)
    static let axDarkslateblue = UIColor(red:72/255,green:61/255,blue:139/255,alpha:1)
    static let axLightslateblue = UIColor(red:132/255,green:112/255,blue:255/255,alpha:1)
    static let axMediumslateblue = UIColor(red:123/255,green:104/255,blue:238/255,alpha:1)
    static let axSlateblue = UIColor(red:106/255,green:90/255,blue:205/255,alpha:1)
    static let axSlateblue1 = UIColor(red:131/255,green:111/255,blue:255/255,alpha:1)
    static let axSlateblue2 = UIColor(red:122/255,green:103/255,blue:238/255,alpha:1)
    static let axSlateblue3 = UIColor(red:105/255,green:89/255,blue:205/255,alpha:1)
    static let axSlateblue4 = UIColor(red:71/255,green:60/255,blue:139/255,alpha:1)
    static let axGhostwhite = UIColor(red:248/255,green:248/255,blue:255/255,alpha:1)
    static let axLavender = UIColor(red:230/255,green:230/255,blue:250/255,alpha:1)
    static let axBlue = UIColor(red:0/255,green:0/255,blue:255/255,alpha:1)
    static let axBlue2 = UIColor(red:0/255,green:0/255,blue:238/255,alpha:1)
    static let axBlue3 = UIColor(red:0/255,green:0/255,blue:205/255,alpha:1)
    static let axBlue5 = UIColor(red:0/255,green:0/255,blue:139/255,alpha:1)
    static let axNavy = UIColor(red:0/255,green:0/255,blue:128/255,alpha:1)
    static let axMidnightblue = UIColor(red:25/255,green:25/255,blue:112/255,alpha:1)
    static let axCobalt = UIColor(red:61/255,green:89/255,blue:171/255,alpha:1)
    static let axRoyalblue = UIColor(red:65/255,green:105/255,blue:225/255,alpha:1)
    static let axRoyalblue1 = UIColor(red:72/255,green:118/255,blue:255/255,alpha:1)
    static let axRoyalblue2 = UIColor(red:67/255,green:110/255,blue:238/255,alpha:1)
    static let axRoyalblue3 = UIColor(red:58/255,green:95/255,blue:205/255,alpha:1)
    static let axRoyalblue4 = UIColor(red:39/255,green:64/255,blue:139/255,alpha:1)
    static let axCornflowerblue = UIColor(red:100/255,green:149/255,blue:237/255,alpha:1)
    static let axLightsteelblue = UIColor(red:176/255,green:196/255,blue:222/255,alpha:1)
    static let axLightsteelblue1 = UIColor(red:202/255,green:225/255,blue:255/255,alpha:1)
    static let axLightsteelblue2 = UIColor(red:188/255,green:210/255,blue:238/255,alpha:1)
    static let axLightsteelblue3 = UIColor(red:162/255,green:181/255,blue:205/255,alpha:1)
    static let axLightsteelblue4 = UIColor(red:110/255,green:123/255,blue:139/255,alpha:1)
    static let axLightslategray = UIColor(red:119/255,green:136/255,blue:153/255,alpha:1)
    static let axSlategray = UIColor(red:112/255,green:128/255,blue:144/255,alpha:1)
    static let axSlategray1 = UIColor(red:198/255,green:226/255,blue:255/255,alpha:1)
    static let axSlategray2 = UIColor(red:185/255,green:211/255,blue:238/255,alpha:1)
    static let axSlategray3 = UIColor(red:159/255,green:182/255,blue:205/255,alpha:1)
    static let axSlategray4 = UIColor(red:108/255,green:123/255,blue:139/255,alpha:1)
    static let axDodgerblue1 = UIColor(red:30/255,green:144/255,blue:255/255,alpha:1)
    static let axDodgerblue2 = UIColor(red:28/255,green:134/255,blue:238/255,alpha:1)
    static let axDodgerblue3 = UIColor(red:24/255,green:116/255,blue:205/255,alpha:1)
    static let axDodgerblue4 = UIColor(red:16/255,green:78/255,blue:139/255,alpha:1)
    static let axAliceblue = UIColor(red:240/255,green:248/255,blue:255/255,alpha:1)
    static let axSteelblue = UIColor(red:70/255,green:130/255,blue:180/255,alpha:1)
    static let axSteelblue1 = UIColor(red:99/255,green:184/255,blue:255/255,alpha:1)
    static let axSteelblue2 = UIColor(red:92/255,green:172/255,blue:238/255,alpha:1)
    static let axSteelblue3 = UIColor(red:79/255,green:148/255,blue:205/255,alpha:1)
    static let axSteelblue4 = UIColor(red:54/255,green:100/255,blue:139/255,alpha:1)
    static let axLightskyblue = UIColor(red:135/255,green:206/255,blue:250/255,alpha:1)
    static let axLightskyblue1 = UIColor(red:176/255,green:226/255,blue:255/255,alpha:1)
    static let axLightskyblue2 = UIColor(red:164/255,green:211/255,blue:238/255,alpha:1)
    static let axLightskyblue3 = UIColor(red:141/255,green:182/255,blue:205/255,alpha:1)
    static let axLightskyblue4 = UIColor(red:96/255,green:123/255,blue:139/255,alpha:1)
    static let axSkyblue1 = UIColor(red:135/255,green:206/255,blue:255/255,alpha:1)
    static let axSkyblue2 = UIColor(red:126/255,green:192/255,blue:238/255,alpha:1)
    static let axSkyblue3 = UIColor(red:108/255,green:166/255,blue:205/255,alpha:1)
    static let axSkyblue4 = UIColor(red:74/255,green:112/255,blue:139/255,alpha:1)
    static let axSkyblue = UIColor(red:135/255,green:206/255,blue:235/255,alpha:1)
    static let axDeepskyblue1 = UIColor(red:0/255,green:191/255,blue:255/255,alpha:1)
    static let axDeepskyblue2 = UIColor(red:0/255,green:178/255,blue:238/255,alpha:1)
    static let axDeepskyblue3 = UIColor(red:0/255,green:154/255,blue:205/255,alpha:1)
    static let axDeepskyblue4 = UIColor(red:0/255,green:104/255,blue:139/255,alpha:1)
    static let axPeacock = UIColor(red:51/255,green:161/255,blue:201/255,alpha:1)
    static let axLightblue = UIColor(red:173/255,green:216/255,blue:230/255,alpha:1)
    static let axLightblue1 = UIColor(red:191/255,green:239/255,blue:255/255,alpha:1)
    static let axLightblue2 = UIColor(red:178/255,green:223/255,blue:238/255,alpha:1)
    static let axLightblue3 = UIColor(red:154/255,green:192/255,blue:205/255,alpha:1)
    static let axLightblue4 = UIColor(red:104/255,green:131/255,blue:139/255,alpha:1)
    static let axPowderblue = UIColor(red:176/255,green:224/255,blue:230/255,alpha:1)
    static let axCadetblue1 = UIColor(red:152/255,green:245/255,blue:255/255,alpha:1)
    static let axCadetblue2 = UIColor(red:142/255,green:229/255,blue:238/255,alpha:1)
    static let axCadetblue3 = UIColor(red:122/255,green:197/255,blue:205/255,alpha:1)
    static let axCadetblue4 = UIColor(red:83/255,green:134/255,blue:139/255,alpha:1)
    static let axTurquoise1 = UIColor(red:0/255,green:245/255,blue:255/255,alpha:1)
    static let axTurquoise2 = UIColor(red:0/255,green:229/255,blue:238/255,alpha:1)
    static let axTurquoise3 = UIColor(red:0/255,green:197/255,blue:205/255,alpha:1)
    static let axTurquoise4 = UIColor(red:0/255,green:134/255,blue:139/255,alpha:1)
    static let axCadetblue = UIColor(red:95/255,green:158/255,blue:160/255,alpha:1)
    static let axDarkturquoise = UIColor(red:0/255,green:206/255,blue:209/255,alpha:1)
    static let axAzure1 = UIColor(red:240/255,green:255/255,blue:255/255,alpha:1)
    static let axAzure2 = UIColor(red:224/255,green:238/255,blue:238/255,alpha:1)
    static let axAzure3 = UIColor(red:193/255,green:205/255,blue:205/255,alpha:1)
    static let axAzure4 = UIColor(red:131/255,green:139/255,blue:139/255,alpha:1)
    static let axLightcyan1 = UIColor(red:224/255,green:255/255,blue:255/255,alpha:1)
    static let axLightcyan2 = UIColor(red:209/255,green:238/255,blue:238/255,alpha:1)
    static let axLightcyan3 = UIColor(red:180/255,green:205/255,blue:205/255,alpha:1)
    static let axLightcyan4 = UIColor(red:122/255,green:139/255,blue:139/255,alpha:1)
    static let axPaleturquoise1 = UIColor(red:187/255,green:255/255,blue:255/255,alpha:1)
    static let axPaleturquoise2 = UIColor(red:174/255,green:238/255,blue:238/255,alpha:1)
    static let axPaleturquoise3 = UIColor(red:150/255,green:205/255,blue:205/255,alpha:1)
    static let axPaleturquoise4 = UIColor(red:102/255,green:139/255,blue:139/255,alpha:1)
    static let axDarkslategray = UIColor(red:47/255,green:79/255,blue:79/255,alpha:1)
    static let axDarkslategray1 = UIColor(red:151/255,green:255/255,blue:255/255,alpha:1)
    static let axDarkslategray2 = UIColor(red:141/255,green:238/255,blue:238/255,alpha:1)
    static let axDarkslategray3 = UIColor(red:121/255,green:205/255,blue:205/255,alpha:1)
    static let axDarkslategray4 = UIColor(red:82/255,green:139/255,blue:139/255,alpha:1)
    static let axCyan = UIColor(red:0/255,green:255/255,blue:255/255,alpha:1)
    static let axCyan2 = UIColor(red:0/255,green:238/255,blue:238/255,alpha:1)
    static let axCyan3 = UIColor(red:0/255,green:205/255,blue:205/255,alpha:1)
    static let axCyan4 = UIColor(red:0/255,green:139/255,blue:139/255,alpha:1)
    static let axTeal = UIColor(red:0/255,green:128/255,blue:128/255,alpha:1)
    static let axMediumturquoise = UIColor(red:72/255,green:209/255,blue:204/255,alpha:1)
    static let axLightseagreen = UIColor(red:32/255,green:178/255,blue:170/255,alpha:1)
    static let axManganeseblue = UIColor(red:3/255,green:168/255,blue:158/255,alpha:1)
    static let axTurquoise = UIColor(red:64/255,green:224/255,blue:208/255,alpha:1)
    static let axColdgrey = UIColor(red:128/255,green:138/255,blue:135/255,alpha:1)
    static let axTurquoiseblue = UIColor(red:0/255,green:199/255,blue:140/255,alpha:1)
    static let axAquamarine1 = UIColor(red:127/255,green:255/255,blue:212/255,alpha:1)
    static let axAquamarine2 = UIColor(red:118/255,green:238/255,blue:198/255,alpha:1)
    static let axAquamarine3 = UIColor(red:102/255,green:205/255,blue:170/255,alpha:1)
    static let axAquamarine4 = UIColor(red:69/255,green:139/255,blue:116/255,alpha:1)
    static let axMediumspringgreen = UIColor(red:0/255,green:250/255,blue:154/255,alpha:1)
    static let axMintcream = UIColor(red:245/255,green:255/255,blue:250/255,alpha:1)
    static let axSpringgreen = UIColor(red:0/255,green:255/255,blue:127/255,alpha:1)
    static let axSpringgreen1 = UIColor(red:0/255,green:238/255,blue:118/255,alpha:1)
    static let axSpringgreen2 = UIColor(red:0/255,green:205/255,blue:102/255,alpha:1)
    static let axSpringgreen3 = UIColor(red:0/255,green:139/255,blue:69/255,alpha:1)
    static let axMediumseagreen = UIColor(red:60/255,green:179/255,blue:113/255,alpha:1)
    static let axSeagreen1 = UIColor(red:84/255,green:255/255,blue:159/255,alpha:1)
    static let axSeagreen2 = UIColor(red:78/255,green:238/255,blue:148/255,alpha:1)
    static let axSeagreen3 = UIColor(red:67/255,green:205/255,blue:128/255,alpha:1)
    static let axSeagreen4 = UIColor(red:46/255,green:139/255,blue:87/255,alpha:1)
    static let axEmeraldgreen = UIColor(red:0/255,green:201/255,blue:87/255,alpha:1)
    static let axMint = UIColor(red:189/255,green:252/255,blue:201/255,alpha:1)
    static let axCobaltgreen = UIColor(red:61/255,green:145/255,blue:64/255,alpha:1)
    static let axHoneydew1 = UIColor(red:240/255,green:255/255,blue:240/255,alpha:1)
    static let axHoneydew2 = UIColor(red:224/255,green:238/255,blue:224/255,alpha:1)
    static let axHoneydew3 = UIColor(red:193/255,green:205/255,blue:193/255,alpha:1)
    static let axHoneydew4 = UIColor(red:131/255,green:139/255,blue:131/255,alpha:1)
    static let axDarkseagreen = UIColor(red:143/255,green:188/255,blue:143/255,alpha:1)
    static let axDarkseagreen1 = UIColor(red:193/255,green:255/255,blue:193/255,alpha:1)
    static let axDarkseagreen2 = UIColor(red:180/255,green:238/255,blue:180/255,alpha:1)
    static let axDarkseagreen3 = UIColor(red:155/255,green:205/255,blue:155/255,alpha:1)
    static let axDarkseagreen4 = UIColor(red:105/255,green:139/255,blue:105/255,alpha:1)
    static let axPalegreen = UIColor(red:152/255,green:251/255,blue:152/255,alpha:1)
    static let axPalegreen1 = UIColor(red:154/255,green:255/255,blue:154/255,alpha:1)
    static let axPalegreen2 = UIColor(red:144/255,green:238/255,blue:144/255,alpha:1)
    static let axPalegreen3 = UIColor(red:124/255,green:205/255,blue:124/255,alpha:1)
    static let axPalegreen4 = UIColor(red:84/255,green:139/255,blue:84/255,alpha:1)
    static let axLimegreen = UIColor(red:50/255,green:205/255,blue:50/255,alpha:1)
    static let axForestgreen = UIColor(red:34/255,green:139/255,blue:34/255,alpha:1)
    static let axGreen1 = UIColor(red:0/255,green:255/255,blue:0/255,alpha:1)
    static let axGreen2 = UIColor(red:0/255,green:238/255,blue:0/255,alpha:1)
    static let axGreen3 = UIColor(red:0/255,green:205/255,blue:0/255,alpha:1)
    static let axGreen4 = UIColor(red:0/255,green:139/255,blue:0/255,alpha:1)
    static let axGreen = UIColor(red:0/255,green:128/255,blue:0/255,alpha:1)
    static let axDarkgreen = UIColor(red:0/255,green:100/255,blue:0/255,alpha:1)
    static let axSapgreen = UIColor(red:48/255,green:128/255,blue:20/255,alpha:1)
    static let axLawngreen = UIColor(red:124/255,green:252/255,blue:0/255,alpha:1)
    static let axChartreuse1 = UIColor(red:127/255,green:255/255,blue:0/255,alpha:1)
    static let axChartreuse2 = UIColor(red:118/255,green:238/255,blue:0/255,alpha:1)
    static let axChartreuse3 = UIColor(red:102/255,green:205/255,blue:0/255,alpha:1)
    static let axChartreuse4 = UIColor(red:69/255,green:139/255,blue:0/255,alpha:1)
    static let axGreenyellow = UIColor(red:173/255,green:255/255,blue:47/255,alpha:1)
    static let axDarkolivegreen1 = UIColor(red:202/255,green:255/255,blue:112/255,alpha:1)
    static let axDarkolivegreen2 = UIColor(red:188/255,green:238/255,blue:104/255,alpha:1)
    static let axDarkolivegreen3 = UIColor(red:162/255,green:205/255,blue:90/255,alpha:1)
    static let axDarkolivegreen4 = UIColor(red:110/255,green:139/255,blue:61/255,alpha:1)
    static let axDarkolivegreen = UIColor(red:85/255,green:107/255,blue:47/255,alpha:1)
    static let axOlivedrab = UIColor(red:107/255,green:142/255,blue:35/255,alpha:1)
    static let axOlivedrab1 = UIColor(red:192/255,green:255/255,blue:62/255,alpha:1)
    static let axOlivedrab2 = UIColor(red:179/255,green:238/255,blue:58/255,alpha:1)
    static let axOlivedrab3 = UIColor(red:154/255,green:205/255,blue:50/255,alpha:1)
    static let axOlivedrab4 = UIColor(red:105/255,green:139/255,blue:34/255,alpha:1)
    static let axIvory1 = UIColor(red:255/255,green:255/255,blue:240/255,alpha:1)
    static let axIvory2 = UIColor(red:238/255,green:238/255,blue:224/255,alpha:1)
    static let axIvory3 = UIColor(red:205/255,green:205/255,blue:193/255,alpha:1)
    static let axIvory4 = UIColor(red:139/255,green:139/255,blue:131/255,alpha:1)
    static let axBeige = UIColor(red:245/255,green:245/255,blue:220/255,alpha:1)
    static let axLightyellow1 = UIColor(red:255/255,green:255/255,blue:224/255,alpha:1)
    static let axLightyellow2 = UIColor(red:238/255,green:238/255,blue:209/255,alpha:1)
    static let axLightyellow3 = UIColor(red:205/255,green:205/255,blue:180/255,alpha:1)
    static let axLightyellow4 = UIColor(red:139/255,green:139/255,blue:122/255,alpha:1)
    static let axLightgoldenrodyellow = UIColor(red:250/255,green:250/255,blue:210/255,alpha:1)
    static let axYellow1 = UIColor(red:255/255,green:255/255,blue:0/255,alpha:1)
    static let axYellow2 = UIColor(red:238/255,green:238/255,blue:0/255,alpha:1)
    static let axYellow3 = UIColor(red:205/255,green:205/255,blue:0/255,alpha:1)
    static let axYellow4 = UIColor(red:139/255,green:139/255,blue:0/255,alpha:1)
    static let axWarmgrey = UIColor(red:128/255,green:128/255,blue:105/255,alpha:1)
    static let axOlive = UIColor(red:128/255,green:128/255,blue:0/255,alpha:1)
    static let axDarkkhaki = UIColor(red:189/255,green:183/255,blue:107/255,alpha:1)
    static let axKhaki1 = UIColor(red:255/255,green:246/255,blue:143/255,alpha:1)
    static let axKhaki2 = UIColor(red:238/255,green:230/255,blue:133/255,alpha:1)
    static let axKhaki3 = UIColor(red:205/255,green:198/255,blue:115/255,alpha:1)
    static let axKhaki4 = UIColor(red:139/255,green:134/255,blue:78/255,alpha:1)
    static let axKhaki = UIColor(red:240/255,green:230/255,blue:140/255,alpha:1)
    static let axPalegoldenrod = UIColor(red:238/255,green:232/255,blue:170/255,alpha:1)
    static let axLemonchiffon1 = UIColor(red:255/255,green:250/255,blue:205/255,alpha:1)
    static let axLemonchiffon2 = UIColor(red:238/255,green:233/255,blue:191/255,alpha:1)
    static let axLemonchiffon3 = UIColor(red:205/255,green:201/255,blue:165/255,alpha:1)
    static let axLemonchiffon4 = UIColor(red:139/255,green:137/255,blue:112/255,alpha:1)
    static let axLightgoldenrod1 = UIColor(red:255/255,green:236/255,blue:139/255,alpha:1)
    static let axLightgoldenrod2 = UIColor(red:238/255,green:220/255,blue:130/255,alpha:1)
    static let axLightgoldenrod3 = UIColor(red:205/255,green:190/255,blue:112/255,alpha:1)
    static let axLightgoldenrod4 = UIColor(red:139/255,green:129/255,blue:76/255,alpha:1)
    static let axBanana = UIColor(red:227/255,green:207/255,blue:87/255,alpha:1)
    static let axGold1 = UIColor(red:255/255,green:215/255,blue:0/255,alpha:1)
    static let axGold2 = UIColor(red:238/255,green:201/255,blue:0/255,alpha:1)
    static let axGold3 = UIColor(red:205/255,green:173/255,blue:0/255,alpha:1)
    static let axGold4 = UIColor(red:139/255,green:117/255,blue:0/255,alpha:1)
    static let axCornsilk1 = UIColor(red:255/255,green:248/255,blue:220/255,alpha:1)
    static let axCornsilk2 = UIColor(red:238/255,green:232/255,blue:205/255,alpha:1)
    static let axCornsilk3 = UIColor(red:205/255,green:200/255,blue:177/255,alpha:1)
    static let axCornsilk4 = UIColor(red:139/255,green:136/255,blue:120/255,alpha:1)
    static let axGoldenrod = UIColor(red:218/255,green:165/255,blue:32/255,alpha:1)
    static let axGoldenrod1 = UIColor(red:255/255,green:193/255,blue:37/255,alpha:1)
    static let axGoldenrod2 = UIColor(red:238/255,green:180/255,blue:34/255,alpha:1)
    static let axGoldenrod3 = UIColor(red:205/255,green:155/255,blue:29/255,alpha:1)
    static let axGoldenrod4 = UIColor(red:139/255,green:105/255,blue:20/255,alpha:1)
    static let axDarkgoldenrod = UIColor(red:184/255,green:134/255,blue:11/255,alpha:1)
    static let axDarkgoldenrod1 = UIColor(red:255/255,green:185/255,blue:15/255,alpha:1)
    static let axDarkgoldenrod2 = UIColor(red:238/255,green:173/255,blue:14/255,alpha:1)
    static let axDarkgoldenrod3 = UIColor(red:205/255,green:149/255,blue:12/255,alpha:1)
    static let axDarkgoldenrod4 = UIColor(red:139/255,green:101/255,blue:8/255,alpha:1)
    static let axOrange1 = UIColor(red:255/255,green:165/255,blue:0/255,alpha:1)
    static let axOrange2 = UIColor(red:238/255,green:154/255,blue:0/255,alpha:1)
    static let axOrange3 = UIColor(red:205/255,green:133/255,blue:0/255,alpha:1)
    static let axOrange4 = UIColor(red:139/255,green:90/255,blue:0/255,alpha:1)
    static let axFloralwhite = UIColor(red:255/255,green:250/255,blue:240/255,alpha:1)
    static let axOldlace = UIColor(red:253/255,green:245/255,blue:230/255,alpha:1)
    static let axWheat = UIColor(red:245/255,green:222/255,blue:179/255,alpha:1)
    static let axWheat1 = UIColor(red:255/255,green:231/255,blue:186/255,alpha:1)
    static let axWheat2 = UIColor(red:238/255,green:216/255,blue:174/255,alpha:1)
    static let axWheat3 = UIColor(red:205/255,green:186/255,blue:150/255,alpha:1)
    static let axWheat4 = UIColor(red:139/255,green:126/255,blue:102/255,alpha:1)
    static let axMoccasin = UIColor(red:255/255,green:228/255,blue:181/255,alpha:1)
    static let axPapayawhip = UIColor(red:255/255,green:239/255,blue:213/255,alpha:1)
    static let axBlanchedalmond = UIColor(red:255/255,green:235/255,blue:205/255,alpha:1)
    static let axNavajowhite1 = UIColor(red:255/255,green:222/255,blue:173/255,alpha:1)
    static let axNavajowhite2 = UIColor(red:238/255,green:207/255,blue:161/255,alpha:1)
    static let axNavajowhite3 = UIColor(red:205/255,green:179/255,blue:139/255,alpha:1)
    static let axNavajowhite4 = UIColor(red:139/255,green:121/255,blue:94/255,alpha:1)
    static let axEggshell = UIColor(red:252/255,green:230/255,blue:201/255,alpha:1)
    static let axTan = UIColor(red:210/255,green:180/255,blue:140/255,alpha:1)
    static let axBrick = UIColor(red:156/255,green:102/255,blue:31/255,alpha:1)
    static let axCadmiumyellow = UIColor(red:255/255,green:153/255,blue:18/255,alpha:1)
    static let axAntiquewhite = UIColor(red:250/255,green:235/255,blue:215/255,alpha:1)
    static let axAntiquewhite1 = UIColor(red:255/255,green:239/255,blue:219/255,alpha:1)
    static let axAntiquewhite2 = UIColor(red:238/255,green:223/255,blue:204/255,alpha:1)
    static let axAntiquewhite3 = UIColor(red:205/255,green:192/255,blue:176/255,alpha:1)
    static let axAntiquewhite4 = UIColor(red:139/255,green:131/255,blue:120/255,alpha:1)
    static let axBurlywood = UIColor(red:222/255,green:184/255,blue:135/255,alpha:1)
    static let axBurlywood1 = UIColor(red:255/255,green:211/255,blue:155/255,alpha:1)
    static let axBurlywood2 = UIColor(red:238/255,green:197/255,blue:145/255,alpha:1)
    static let axBurlywood3 = UIColor(red:205/255,green:170/255,blue:125/255,alpha:1)
    static let axBurlywood4 = UIColor(red:139/255,green:115/255,blue:85/255,alpha:1)
    static let axBisque1 = UIColor(red:255/255,green:228/255,blue:196/255,alpha:1)
    static let axBisque2 = UIColor(red:238/255,green:213/255,blue:183/255,alpha:1)
    static let axBisque3 = UIColor(red:205/255,green:183/255,blue:158/255,alpha:1)
    static let axBisque4 = UIColor(red:139/255,green:125/255,blue:107/255,alpha:1)
    static let axMelon = UIColor(red:227/255,green:168/255,blue:105/255,alpha:1)
    static let axCarrot = UIColor(red:237/255,green:145/255,blue:33/255,alpha:1)
    static let axDarkorange = UIColor(red:255/255,green:140/255,blue:0/255,alpha:1)
    static let axDarkorange1 = UIColor(red:255/255,green:127/255,blue:0/255,alpha:1)
    static let axDarkorange2 = UIColor(red:238/255,green:118/255,blue:0/255,alpha:1)
    static let axDarkorange3 = UIColor(red:205/255,green:102/255,blue:0/255,alpha:1)
    static let axDarkorange4 = UIColor(red:139/255,green:69/255,blue:0/255,alpha:1)
    static let axOrange = UIColor(red:255/255,green:128/255,blue:0/255,alpha:1)
    static let axTan1 = UIColor(red:255/255,green:165/255,blue:79/255,alpha:1)
    static let axTan2 = UIColor(red:238/255,green:154/255,blue:73/255,alpha:1)
    static let axTan3 = UIColor(red:205/255,green:133/255,blue:63/255,alpha:1)
    static let axTan4 = UIColor(red:139/255,green:90/255,blue:43/255,alpha:1)
    static let axLinen = UIColor(red:250/255,green:240/255,blue:230/255,alpha:1)
    static let axPeachpuff1 = UIColor(red:255/255,green:218/255,blue:185/255,alpha:1)
    static let axPeachpuff2 = UIColor(red:238/255,green:203/255,blue:173/255,alpha:1)
    static let axPeachpuff3 = UIColor(red:205/255,green:175/255,blue:149/255,alpha:1)
    static let axPeachpuff4 = UIColor(red:139/255,green:119/255,blue:101/255,alpha:1)
    static let axSeashell1 = UIColor(red:255/255,green:245/255,blue:238/255,alpha:1)
    static let axSeashell2 = UIColor(red:238/255,green:229/255,blue:222/255,alpha:1)
    static let axSeashell3 = UIColor(red:205/255,green:197/255,blue:191/255,alpha:1)
    static let axSeashell4 = UIColor(red:139/255,green:134/255,blue:130/255,alpha:1)
    static let axSandybrown = UIColor(red:244/255,green:164/255,blue:96/255,alpha:1)
    static let axRawsienna = UIColor(red:199/255,green:97/255,blue:20/255,alpha:1)
    static let axChocolate = UIColor(red:210/255,green:105/255,blue:30/255,alpha:1)
    static let axChocolate1 = UIColor(red:255/255,green:127/255,blue:36/255,alpha:1)
    static let axChocolate2 = UIColor(red:238/255,green:118/255,blue:33/255,alpha:1)
    static let axChocolate3 = UIColor(red:205/255,green:102/255,blue:29/255,alpha:1)
    static let axChocolate4 = UIColor(red:139/255,green:69/255,blue:19/255,alpha:1)
    static let axIvoryblack = UIColor(red:41/255,green:36/255,blue:33/255,alpha:1)
    static let axFlesh = UIColor(red:255/255,green:125/255,blue:64/255,alpha:1)
    static let axCadmiumorange = UIColor(red:255/255,green:97/255,blue:3/255,alpha:1)
    static let axBurntsienna = UIColor(red:138/255,green:54/255,blue:15/255,alpha:1)
    static let axSienna = UIColor(red:160/255,green:82/255,blue:45/255,alpha:1)
    static let axSienna1 = UIColor(red:255/255,green:130/255,blue:71/255,alpha:1)
    static let axSienna2 = UIColor(red:238/255,green:121/255,blue:66/255,alpha:1)
    static let axSienna3 = UIColor(red:205/255,green:104/255,blue:57/255,alpha:1)
    static let axSienna4 = UIColor(red:139/255,green:71/255,blue:38/255,alpha:1)
    static let axLightsalmon1 = UIColor(red :255/255,green:160/255,blue:122/255,alpha:1)
    static let axLightsalmon2 = UIColor(red:238/255,green:149/255,blue:114/255,alpha:1)
    static let axLightsalmon3 = UIColor(red:205/255,green:129/255,blue:98/255,alpha:1)
    static let axLightsalmon4 = UIColor(red:139/255,green:87/255,blue:66/255,alpha:1)
    static let axCoral = UIColor(red:255/255,green:127/255,blue:80/255,alpha:1)
    static let axOrangered1 = UIColor(red:255/255,green:69/255,blue:0/255,alpha:1)
    static let axOrangered2 = UIColor(red:238/255,green:64/255,blue:0/255,alpha:1)
    static let axOrangered3 = UIColor(red:205/255,green:55/255,blue:0/255,alpha:1)
    static let axOrangered4 = UIColor(red:139/255,green:37/255,blue:0/255,alpha:1)
    static let axSepia = UIColor(red:94/255,green:38/255,blue:18/255,alpha:1)
    static let axDarksalmon = UIColor(red:233/255,green:150/255,blue:122/255,alpha:1)
    static let axSalmon1 = UIColor(red:255/255,green:140/255,blue:105/255,alpha:1)
    static let axSalmon2 = UIColor(red:238/255,green:130/255,blue:98/255,alpha:1)
    static let axSalmon3 = UIColor(red:205/255,green:112/255,blue:84/255,alpha:1)
    static let axSalmon4 = UIColor(red:139/255,green:76/255,blue:57/255,alpha:1)
    static let axCoral1 = UIColor(red:255/255,green:114/255,blue:86/255,alpha:1)
    static let axCoral2 = UIColor(red:238/255,green:106/255,blue:80/255,alpha:1)
    static let axCoral3 = UIColor(red:205/255,green:91/255,blue:69/255,alpha:1)
    static let axCoral4 = UIColor(red:139/255,green:62/255,blue:47/255,alpha:1)
    static let axBurntumber = UIColor(red:138/255,green:51/255,blue:36/255,alpha:1)
    static let axTomato1 = UIColor(red:255/255,green:99/255,blue:71/255,alpha:1)
    static let axTomato2 = UIColor(red:238/255,green:92/255,blue:66/255,alpha:1)
    static let axTomato3 = UIColor(red:205/255,green:79/255,blue:57/255,alpha:1)
    static let axTomato4 = UIColor(red:139/255,green:54/255,blue:38/255,alpha:1)
    static let axSalmon = UIColor(red:250/255,green:128/255,blue:114/255,alpha:1)
    static let axMistyrose1 = UIColor(red:255/255,green:228/255,blue:225/255,alpha:1)
    static let axMistyrose2 = UIColor(red:238/255,green:213/255,blue:210/255,alpha:1)
    static let axMistyrose3 = UIColor(red:205/255,green:183/255,blue:181/255,alpha:1)
    static let axMistyrose4 = UIColor(red:139/255,green:125/255,blue:123/255,alpha:1)
    static let axSnow1 = UIColor(red:255/255,green:250/255,blue:250/255,alpha:1)
    static let axSnow2 = UIColor(red:238/255,green:233/255,blue:233/255,alpha:1)
    static let axSnow3 = UIColor(red:205/255,green:201/255,blue:201/255,alpha:1)
    static let axSnow4 = UIColor(red:139/255,green:137/255,blue:137/255,alpha:1)
    static let axRosybrown = UIColor(red:188/255,green:143/255,blue:143/255,alpha:1)
    static let axRosybrown1 = UIColor(red:255/255,green:193/255,blue:193/255,alpha:1)
    static let axRosybrown2 = UIColor(red:238/255,green:180/255,blue:180/255,alpha:1)
    static let axRosybrown3 = UIColor(red:205/255,green:155/255,blue:155/255,alpha:1)
    static let axRosybrown4 = UIColor(red:139/255,green:105/255,blue:105/255,alpha:1)
    static let axLightcoral = UIColor(red:240/255,green:128/255,blue:128/255,alpha:1)
    static let axIndianred1 = UIColor(red:255/255,green:106/255,blue:106/255,alpha:1)
    static let axIndianred2 = UIColor(red:238/255,green:99/255,blue:99/255,alpha:1)
    static let axIndianred4 = UIColor(red:139/255,green:58/255,blue:58/255,alpha:1)
    static let axIndianred3 = UIColor(red:205/255,green:85/255,blue:85/255,alpha:1)
    static let axBrown = UIColor(red:165/255,green:42/255,blue:42/255,alpha:1)
    static let axBrown1 = UIColor(red:255/255,green:64/255,blue:64/255,alpha:1)
    static let axBrown2 = UIColor(red:238/255,green:59/255,blue:59/255,alpha:1)
    static let axBrown3 = UIColor(red:205/255,green:51/255,blue:51/255,alpha:1)
    static let axBrown4 = UIColor(red:139/255,green:35/255,blue:35/255,alpha:1)
    static let axFirebrick = UIColor(red:178/255,green:34/255,blue:34/255,alpha:1)
    static let axFirebrick1 = UIColor(red:255/255,green:48/255,blue:48/255,alpha:1)
    static let axFirebrick2 = UIColor(red:238/255,green:44/255,blue:44/255,alpha:1)
    static let axFirebrick3 = UIColor(red:205/255,green:38/255,blue:38/255,alpha:1)
    static let axFirebrick4 = UIColor(red:139/255,green:26/255,blue:26/255,alpha:1)
    static let axRed1 = UIColor(red:255/255,green:0/255,blue:0/255,alpha:1)
    static let axRed2 = UIColor(red:238/255,green:0/255,blue:0/255,alpha:1)
    static let axRed3 = UIColor(red:205/255,green:0/255,blue:0/255,alpha:1)
    static let axRed4 = UIColor(red:139/255,green:0/255,blue:0/255,alpha:1)
    static let axMaroon = UIColor(red:128/255,green:0/255,blue:0/255,alpha:1)
    static let axSgibeet = UIColor(red:142/255,green:56/255,blue:142/255,alpha:1)
    static let axSgislateblue = UIColor(red:113/255,green:113/255,blue:198/255,alpha:1)
    static let axSgilightblue = UIColor(red:125/255,green:158/255,blue:192/255,alpha:1)
    static let axSgiteal = UIColor(red:56/255,green:142/255,blue:142/255,alpha:1)
    static let axSgichartreuse = UIColor(red:113/255,green:198/255,blue:113/255,alpha:1)
    static let axSgiolivedrab = UIColor(red:142/255,green:142/255,blue:56/255,alpha:1)
    static let axSgibrightgray = UIColor(red:197/255,green:193/255,blue:170/255,alpha:1)
    static let axSgisalmon = UIColor(red:198/255,green:113/255,blue:113/255,alpha:1)
    static let axSgidarkgray = UIColor(red:85/255,green:85/255,blue:85/255,alpha:1)
    static let axSgigray12 = UIColor(red:30/255,green:30/255,blue:30/255,alpha:1)
    static let axSgigray16 = UIColor(red:40/255,green:40/255,blue:40/255,alpha:1)
    static let axSgigray32 = UIColor(red:81/255,green:81/255,blue:81/255,alpha:1)
    static let axSgigray36 = UIColor(red:91/255,green:91/255,blue:91/255,alpha:1)
    static let axSgigray52 = UIColor(red:132/255,green:132/255,blue:132/255,alpha:1)
    static let axSgigray56 = UIColor(red:142/255,green:142/255,blue:142/255,alpha:1)
    static let axSgilightgray = UIColor(red:170/255,green:170/255,blue:170/255,alpha:1)
    static let axSgigray72 = UIColor(red:183/255,green:183/255,blue:183/255,alpha:1)
    static let axSgigray76 = UIColor(red:193/255,green:193/255,blue:193/255,alpha:1)
    static let axSgigray92 = UIColor(red:234/255,green:234/255,blue:234/255,alpha:1)
    static let axSgigray96 = UIColor(red:244/255,green:244/255,blue:244/255,alpha:1)
    static let axWhite = UIColor(red:255/255,green:255/255,blue:255/255,alpha:1)
    static let axWhitesmoke = UIColor(red:245/255,green:245/255,blue:245/255,alpha:1)
    static let axGainsboro = UIColor(red:220/255,green:220/255,blue:220/255,alpha:1)
    static let axLightgrey = UIColor(red:211/255,green:211/255,blue:211/255,alpha:1)
    static let axSilver = UIColor(red:192/255,green:192/255,blue:192/255,alpha:1)
    static let axDarkgray = UIColor(red:169/255,green:169/255,blue:169/255,alpha:1)
    static let axGray = UIColor(red:128/255,green:128/255,blue:128/255,alpha:1)
    static let axDimgray = UIColor(red:105/255,green:105/255,blue:105/255,alpha:1)
    static let axBlack = UIColor(red:0/255,green:0/255,blue:0/255,alpha:1)
    static let axGray99 = UIColor(red:252/255,green:252/255,blue:252/255,alpha:1)
    static let axGray98 = UIColor(red:250/255,green:250/255,blue:250/255,alpha:1)
    static let axGray97 = UIColor(red:247/255,green:247/255,blue:247/255,alpha:1)
    static let axWhitesmok = UIColor(red:245/255,green:245/255,blue:245/255,alpha:1)
    static let axGray95 = UIColor(red:242/255,green:242/255,blue:242/255,alpha:1)
    static let axGray94 = UIColor(red:240/255,green:240/255,blue:240/255,alpha:1)
    static let axGray93 = UIColor(red:237/255,green:237/255,blue:237/255,alpha:1)
    static let axGray92 = UIColor(red:235/255,green:235/255,blue:235/255,alpha:1)
    static let axGray91 = UIColor(red:232/255,green:232/255,blue:232/255,alpha:1)
    static let axGray90 = UIColor(red:229/255,green:229/255,blue:229/255,alpha:1)
    static let axGray89 = UIColor(red:227/255,green:227/255,blue:227/255,alpha:1)
    static let axGray88 = UIColor(red:224/255,green:224/255,blue:224/255,alpha:1)
    static let axGray87 = UIColor(red:222/255,green:222/255,blue:222/255,alpha:1)
    static let axGray86 = UIColor(red:219/255,green:219/255,blue:219/255,alpha:1)
    static let axGray85 = UIColor(red:217/255,green:217/255,blue:217/255,alpha:1)
    static let axGray84 = UIColor(red:214/255,green:214/255,blue:214/255,alpha:1)
    static let axGray83 = UIColor(red:212/255,green:212/255,blue:212/255,alpha:1)
    static let axGray82 = UIColor(red:209/255,green:209/255,blue:209/255,alpha:1)
    static let axGray81 = UIColor(red:207/255,green:207/255,blue:207/255,alpha:1)
    static let axGray80 = UIColor(red:204/255,green:204/255,blue:204/255,alpha:1)
    static let axGray79 = UIColor(red:201/255,green:201/255,blue:201/255,alpha:1)
    static let axGray78 = UIColor(red:199/255,green:199/255,blue:199/255,alpha:1)
    static let axGray77 = UIColor(red:196/255,green:196/255,blue:196/255,alpha:1)
    static let axGray76 = UIColor(red:194/255,green:194/255,blue:194/255,alpha:1)
    static let axGray75 = UIColor(red:191/255,green:191/255,blue:191/255,alpha:1)
    static let axGray74 = UIColor(red:189/255,green:189/255,blue:189/255,alpha:1)
    static let axGray73 = UIColor(red:186/255,green:186/255,blue:186/255,alpha:1)
    static let axGray72 = UIColor(red:184/255,green:184/255,blue:184/255,alpha:1)
    static let axGray71 = UIColor(red:181/255,green:181/255,blue:181/255,alpha:1)
    static let axGray70 = UIColor(red:179/255,green:179/255,blue:179/255,alpha:1)
    static let axGray69 = UIColor(red:176/255,green:176/255,blue:176/255,alpha:1)
    static let axGray68 = UIColor(red:173/255,green:173/255,blue:173/255,alpha:1)
    static let axGray67 = UIColor(red:171/255,green:171/255,blue:171/255,alpha:1)
    static let axGray66 = UIColor(red:168/255,green:168/255,blue:168/255,alpha:1)
    static let axGray65 = UIColor(red:166/255,green:166/255,blue:166/255,alpha:1)
    static let axGray64 = UIColor(red:163/255,green:163/255,blue:163/255,alpha:1)
    static let axGray63 = UIColor(red:161/255,green:161/255,blue:161/255,alpha:1)
    static let axGray62 = UIColor(red:158/255,green:158/255,blue:158/255,alpha:1)
    static let axGray61 = UIColor(red:156/255,green:156/255,blue:156/255,alpha:1)
    static let axGray60 = UIColor(red:153/255,green:153/255,blue:153/255,alpha:1)
    static let axGray59 = UIColor(red:150/255,green:150/255,blue:150/255,alpha:1)
    static let axGray58 = UIColor(red:148/255,green:148/255,blue:148/255,alpha:1)
    static let axGray57 = UIColor(red:145/255,green:145/255,blue:145/255,alpha:1)
    static let axGray56 = UIColor(red:143/255,green:143/255,blue:143/255,alpha:1)
    static let axGray55 = UIColor(red:140/255,green:140/255,blue:140/255,alpha:1)
    static let axGray54 = UIColor(red:138/255,green:138/255,blue:138/255,alpha:1)
    static let axGray53 = UIColor(red:135/255,green:135/255,blue:135/255,alpha:1)
    static let axGray52 = UIColor(red:133/255,green:133/255,blue:133/255,alpha:1)
    static let axGray51 = UIColor(red:130/255,green:130/255,blue:130/255,alpha:1)
    static let axGray50 = UIColor(red:127/255,green:127/255,blue:127/255,alpha:1)
    static let axGray49 = UIColor(red:125/255,green:125/255,blue:125/255,alpha:1)
    static let axGray48 = UIColor(red:122/255,green:122/255,blue:122/255,alpha:1)
    static let axGray47 = UIColor(red:120/255,green:120/255,blue:120/255,alpha:1)
    static let axGray46 = UIColor(red:117/255,green:117/255,blue:117/255,alpha:1)
    static let axGray45 = UIColor(red:115/255,green:115/255,blue:115/255,alpha:1)
    static let axGray44 = UIColor(red:112/255,green:112/255,blue:112/255,alpha:1)
    static let axGray43 = UIColor(red:110/255,green:110/255,blue:110/255,alpha:1)
    static let axGray42 = UIColor(red:107/255,green:107/255,blue:107/255,alpha:1)
    static let axGray40 = UIColor(red:102/255,green:102/255,blue:102/255,alpha:1)
    static let axGray39 = UIColor(red:99/255,green:99/255,blue:99/255,alpha:1)
    static let axGray38 = UIColor(red:97/255,green:97/255,blue:97/255,alpha:1)
    static let axGray37 = UIColor(red:94/255,green:94/255,blue:94/255,alpha:1)
    static let axGray36 = UIColor(red:92/255,green:92/255,blue:92/255,alpha:1)
    static let axGray35 = UIColor(red:89/255,green:89/255,blue:89/255,alpha:1)
    static let axGray34 = UIColor(red:87/255,green:87/255,blue:87/255,alpha:1)
    static let axGray33 = UIColor(red:84/255,green:84/255,blue:84/255,alpha:1)
    static let axGray32 = UIColor(red:82/255,green:82/255,blue:82/255,alpha:1)
    static let axGray31 = UIColor(red:79/255,green:79/255,blue:79/255,alpha:1)
    static let axGray30 = UIColor(red:77/255,green:77/255,blue:77/255,alpha:1)
    static let axGray29 = UIColor(red:74/255,green:74/255,blue:74/255,alpha:1)
    static let axGray28 = UIColor(red:71/255,green:71/255,blue:71/255,alpha:1)
    static let axGray27 = UIColor(red:69/255,green:69/255,blue:69/255,alpha:1)
    static let axGray26 = UIColor(red:66/255,green:66/255,blue:66/255,alpha:1)
    static let axGray25 = UIColor(red:64/255,green:64/255,blue:64/255,alpha:1)
    static let axGray24 = UIColor(red:61/255,green:61/255,blue:61/255,alpha:1)
    static let axGray23 = UIColor(red:59/255,green:59/255,blue:59/255,alpha:1)
    static let axGray22 = UIColor(red:56/255,green:56/255,blue:56/255,alpha:1)
    static let axGray21 = UIColor(red:54/255,green:54/255,blue:54/255,alpha:1)
    static let axGray20 = UIColor(red:51/255,green:51/255,blue:51/255,alpha:1)
    static let axGray19 = UIColor(red:48/255,green:48/255,blue:48/255,alpha:1)
    static let axGray18 = UIColor(red:46/255,green:46/255,blue:46/255,alpha:1)
    static let axGray17 = UIColor(red:43/255,green:43/255,blue:43/255,alpha:1)
    static let axGray16 = UIColor(red:41/255,green:41/255,blue:41/255,alpha:1)
    static let axGray15 = UIColor(red:38/255,green:38/255,blue:38/255,alpha:1)
    static let axGray14 = UIColor(red:36/255,green:36/255,blue:36/255,alpha:1)
    static let axGray13 = UIColor(red:33/255,green:33/255,blue:33/255,alpha:1)
    static let axGray12 = UIColor(red:31/255,green:31/255,blue:31/255,alpha:1)
    static let axGray11 = UIColor(red:28/255,green:28/255,blue:28/255,alpha:1)
    static let axGray10 = UIColor(red:26/255,green:26/255,blue:26/255,alpha:1)
    static let axGray9 = UIColor(red:23/255,green:23/255,blue:23/255,alpha:1)
    static let axGray8 = UIColor(red:20/255,green:20/255,blue:20/255,alpha:1)
    static let axGray7 = UIColor(red:18/255,green:18/255,blue:18/255,alpha:1)
    static let axGray6 = UIColor(red:15/255,green:15/255,blue:15/255,alpha:1)
    static let axGray5 = UIColor(red:13/255,green:13/255,blue:13/255,alpha:1)
    static let axGray4 = UIColor(red:10/255,green:10/255,blue:10/255,alpha:1)
    static let axGray3 = UIColor(red:8/255,green:8/255,blue:8/255,alpha:1)
    static let axGray2 = UIColor(red:5/255,green:5/255,blue:5/255,alpha:1)
    static let axGray1 = UIColor(red:3/255,green:3/255,blue:3/255,alpha:1)
}

extension UIColor {
    static let axTeal001 = UIColor(red: 48/255, green: 164/255, blue: 182/255, alpha: 1) //core
    static let axRed001 = UIColor(red: 247/55, green: 66/255, blue: 82/255, alpha: 1) // core
    static let axBlue001 = UIColor(red: 9/255, green: 45/255, blue: 64/255, alpha: 1) // core
    static let axBlue002 = UIColor(red: 218/255, green: 235/255, blue: 243/255, alpha: 1) // core
    //  static let axBlue03 = UIColor(red: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: <#T##CGFloat#>)
    static let axWhite001 = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
    static let axBlack001 = UIColor.black
    static let axBlack002 = UIColor.black
}
//https://brandpalettes.com/aldi-color-codes/

extension UIColor {
    static let axBrandAmazonYellow = UIColor(red: 255/255, green: 153/255, blue: 0, alpha: 1)
    static let axBrandAmazonBlack = UIColor.black
    static let axBrandAirBnBCoral = UIColor(red: 255/255, green: 83/255, blue: 93/255, alpha: 1)
    static let axBrandAldiWhite = UIColor.white
    
    
}


enum FontNames:String {
    
    case CopperplateLight = "Copperplate-Light"
    case Copperplate = "Copperplate"
    case CopperplateBold = "CopperplateLight"
    case KohinoorTeluguRegular = "KohinoorTelugu-Regular"
    case KohinoorTeluguMedium = "KohinoorTelugu-Medium"
    case KohinoorTeluguLight = "KohinoorTelugu-Light"
    case Thonburi = "Thonburi"
    case ThonburiBold = "Thonburi-Bold"
    case ThonburiLight = "Thonburi-Light"
    case CourierNewPSBoldMT = "CourierNewPS-BoldMT"
    case CourierNewPSItalicMT = "CourierNewPS-ItalicMT"
    case CourierNewPSMT = "CourierNewPSMT"
    case CourierNewPSBoldItalicMT = "CourierNewPS-BoldItalicMT"
    case GillSansItalic = "GillSans-Italic"
    case GillSansBold = "GillSans-Bold"
    case GillSansBoldItalic = "GillSans-BoldItalic"
    case GillSansLightItalic = "GillSans-LightItalic"
    case GillSans = "GillSans"
    case GillSansLight = "GillSans-Light"
    case GillSansSemiBold = "GillSans-SemiBold"
    case GillSansSemiBoldItalic = "GillSans-SemiBoldItalic"
    case GillSansUltraBold = "GillSans-UltraBold"
    case AppleSDGothicNeoBold = "AppleSDGothicNeo-Bold"
    case AppleSDGothicNeoUltraLight = "AppleSDGothicNeo-UltraLight"
    case AppleSDGothicNeoThin = "AppleSDGothicNeo-Thin"
    case AppleSDGothicNeoRegular = "AppleSDGothicNeo-Regular"
    case AppleSDGothicNeoLight = "AppleSDGothicNeo-Light"
    case AppleSDGothicNeoMedium = "AppleSDGothicNeo-Medium"
    case AppleSDGothicNeoSemiBold = "AppleSDGothicNeo-SemiBold"
    case MarkerFeltThin = "MarkerFelt-Thin"
    case MarkerFeltWide = "MarkerFelt-Wide"
    case AvenirNextCondensedBoldItalic = "AvenirNextCondensed-BoldItalic"
    case AvenirNextCondensedHeavy = "AvenirNextCondensed-Heavy"
    case AvenirNextCondensedMedium = "AvenirNextCondensed-Medium"
    case AvenirNextCondensedRegular = "AvenirNextCondensed-Regular"
    case AvenirNextCondensedHeavyItalic = "AvenirNextCondensed-HeavyItalic"
    case AvenirNextCondensedMediumItalic = "AvenirNextCondensed-MediumItalic"
    case AvenirNextCondensedItalic = "AvenirNextCondensed-Italic"
    case AvenirNextCondensedUltraLightItalic = "AvenirNextCondensed-UltraLightItalic"
    case AvenirNextCondensedUltraLight = "AvenirNextCondensed-UltraLight"
    case AvenirNextCondensedDemiBold = "AvenirNextCondensed-DemiBold"
    case AvenirNextCondensedBold = "AvenirNextCondensed-Bold"
    case AvenirNextCondensedDemiBoldItalic = "AvenirNextCondensed-DemiBoldItalic"
    case TamilSangamMN = "TamilSangamMN"
    case TamilSangamMNBold = "TamilSangamMN-Bold"
    case HelveticaNeueItalic = "HelveticaNeue-Italic"
    case HelveticaNeueBold = "HelveticaNeue-Bold"
    case HelveticaNeueUltraLight = "HelveticaNeue-UltraLight"
    case HelveticaNeueCondensedBlack = "HelveticaNeue-CondensedBlack"
    case HelveticaNeueBoldItalic = "HelveticaNeue-BoldItalic"
    case HelveticaNeueCondensedBold = "HelveticaNeue-CondensedBold"
    case HelveticaNeueMedium = "HelveticaNeue-Medium"
    case HelveticaNeueLight = "HelveticaNeue-Light"
    case HelveticaNeueThin = "HelveticaNeue-Thin"
    case HelveticaNeueThinItalic = "HelveticaNeue-ThinItalic"
    case HelveticaNeueLightItalic = "HelveticaNeue-LightItalic"
    case HelveticaNeueUltraLightItalic = "HelveticaNeue-UltraLightItalic"
    case HelveticaNeueMediumItalic = "HelveticaNeue-MediumItalic"
    case HelveticaNeue = "HelveticaNeue"
    case GurmukhiMNBold = "GurmukhiMN-Bold"
    case GurmukhiMN = "GurmukhiMN"
    case TimesNewRomanPSMT = "TimesNewRomanPSMT"
    case TimesNewRomanPSBoldItalicMT = "TimesNewRomanPS-BoldItalicMT"
    case TimesNewRomanPSItalicMT = "TimesNewRomanPS-ItalicMT"
    case TimesNewRomanPSBoldMT = "TimesNewRomanPS-BoldMT"
    case GeorgiaBoldItalic = "Georgia-BoldItalic"
    case Georgia = "Georgia"
    case GeorgiaItalic = "Georgia-Italic"
    case GeorgiaBold = "Georgia-Bold"
    case AppleColorEmoji = "AppleColorEmoji"
    case ArialRoundedMTBold = "ArialRoundedMTBold"
    case KailasaBold = "Kailasa-Bold"
    case Kailasa = "Kailasa"
    case KohinoorDevanagariLight = "KohinoorDevanagari-Light"
    case KohinoorDevanagariRegular = "KohinoorDevanagari-Regular"
    case KohinoorDevanagariSemibold = "KohinoorDevanagari-Semibold"
    case KohinoorBanglaSemibold = "KohinoorBangla-Semibold"
    case KohinoorBanglaRegular = "KohinoorBangla-Regular"
    case KohinoorBanglaLight = "KohinoorBangla-Light"
    case ChalkboardSEBold = "ChalkboardSE-Bold"
    case ChalkboardSELight = "ChalkboardSE-Light"
    case ChalkboardSERegular = "ChalkboardSE-Regular"
    case SinhalaSangamMNBold = "SinhalaSangamMN-Bold"
    case SinhalaSangamMN = "SinhalaSangamMN"
    case PingFangTCMedium = "PingFangTC-Medium"
    case PingFangTCRegular = "PingFangTC-Regular"
    case PingFangTCLight = "PingFangTC-Light"
    case PingFangTCUltralight = "PingFangTC-Ultralight"
    case PingFangTCSemibold = "PingFangTC-Semibold"
    case PingFangTCThin = "PingFangTC-Thin"
    case GujaratiSangamMNBold = "GujaratiSangamMN-Bold"
    case GujaratiSangamMN = "GujaratiSangamMN"
    case DamascusLight = "DamascusLight"
    case DamascusBold = "DamascusBold"
    case DamascusSemiBold = "DamascusSemiBold"
    case DamascusMedium = "DamascusMedium"
    case Damascus = "Damascus"
    case NoteworthyLight = "Noteworthy-Light"
    case NoteworthyBold = "Noteworthy-Bold"
    case GeezaPro = "GeezaPro"
    case GeezaProBold = "GeezaPro-Bold"
    case AvenirMedium = "Avenir-Medium"
    case AvenirHeavyOblique = "Avenir-HeavyOblique"
    case AvenirBook = "Avenir-Book"
    case AvenirLight = "Avenir-Light"
    case AvenirRoman = "Avenir-Roman"
    case AvenirBookOblique = "Avenir-BookOblique"
    case AvenirMediumOblique = "Avenir-MediumOblique"
    case AvenirBlack = "Avenir-Black"
    case AvenirBlackOblique = "Avenir-BlackOblique"
    case AvenirHeavy = "Avenir-Heavy"
    case AvenirLightOblique = "Avenir-LightOblique"
    case AvenirOblique = "Avenir-Oblique"
    case AcademyEngravedLetPlain = "AcademyEngravedLetPlain"
    case DiwanMishafi = "DiwanMishafi"
    case FuturaCondensedMedium = "Futura-CondensedMedium"
    case FuturaCondensedExtraBold = "Futura-CondensedExtraBold"
    case FuturaMedium = "Futura-Medium"
    case FuturaMediumItalic = "Futura-MediumItalic"
    case FuturaBold = "Futura-Bold"
    case Farah = "Farah"
    case KannadaSangamMN = "KannadaSangamMN"
    case KannadaSangamMNBold = "KannadaSangamMN-Bold"
    case ArialHebrewBold = "ArialHebrew-Bold"
    case ArialHebrewLight = "ArialHebrew-Light"
    case ArialHebrew = "ArialHebrew"
    case ArialMT = "ArialMT"
    case ArialBoldItalicMT = "Arial-BoldItalicMT"
    case ArialBoldMT = "Arial-BoldMT"
    case ArialItalicMT = "Arial-ItalicMT"
    case PartyLetPlain = "PartyLetPlain"
    case Chalkduster = "Chalkduster"
    case HoeflerTextItalic = "HoeflerText-Italic"
    case HoeflerTextRegular = "HoeflerText-Regular"
    case HoeflerTextBlack = "HoeflerText-Black"
    case HoeflerTextBlackItalic = "HoeflerText-BlackItalic"
    case OptimaRegular = "Optima-Regular"
    case OptimaExtraBlack = "Optima-ExtraBlack"
    case OptimaBoldItalic = "Optima-BoldItalic"
    case OptimaItalic = "Optima-Italic"
    case OptimaBold = "Optima-Bold"
    case PalatinoBold = "Palatino-Bold"
    case PalatinoRoman = "Palatino-Roman"
    case PalatinoBoldItalic = "Palatino-BoldItalic"
    case PalatinoItalic = "Palatino-Italic"
    case LaoSangamMN = "LaoSangamMN"
    case MalayalamSangamMNBold = "MalayalamSangamMN-Bold"
    case MalayalamSangamMN = "MalayalamSangamMN"
    case AlNileBold = "AlNile-Bold"
    case AlNile = "AlNile"
    case BradleyHandITCTTBold = "BradleyHandITCTT-Bold"
    case PingFangHKUltralight = "PingFangHK-Ultralight"
    case PingFangHKSemibold = "PingFangHK-Semibold"
    case PingFangHKThin = "PingFangHK-Thin"
    case PingFangHKLight = "PingFangHK-Light"
    case PingFangHKRegular = "PingFangHK-Regular"
    case PingFangHKMedium = "PingFangHK-Medium"
    case TrebuchetBoldItalic = "Trebuchet-BoldItalic"
    case TrebuchetMS = "TrebuchetMS"
    case TrebuchetMSBold = "TrebuchetMS-Bold"
    case TrebuchetMSItalic = "TrebuchetMS-Italic"
    case HelveticaBold = "Helvetica-Bold"
    case Helvetica = "Helvetica"
    case HelveticaLightOblique = "Helvetica-LightOblique"
    case HelveticaOblique = "Helvetica-Oblique"
    case HelveticaBoldOblique = "Helvetica-BoldOblique"
    case HelveticaLight = "Helvetica-Light"
    case CourierBoldOblique = "Courier-BoldOblique"
    case Courier = "Courier"
    case CourierBold = "Courier-Bold"
    case CourierOblique = "Courier-Oblique"
    case CochinBold = "Cochin-Bold"
    case Cochin = "Cochin"
    case CochinItalic = "Cochin-Italic"
    case CochinBoldItalic = "Cochin-BoldItalic"
    case HiraMinProNW6 = "HiraMinProN-W6"
    case HiraMinProNW3 = "HiraMinProN-W3"
    case DevanagariSangamMN = "DevanagariSangamMN"
    case DevanagariSangamMNBold = "DevanagariSangamMN-Bold"
    case OriyaSangamMN = "OriyaSangamMN"
    case OriyaSangamMNBold = "OriyaSangamMN-Bold"
    case SnellRoundhandBold = "SnellRoundhand-Bold"
    case SnellRoundhand = "SnellRoundhand"
    case SnellRoundhandBlack = "SnellRoundhand-Black"
    case ZapfDingbatsITC = "ZapfDingbatsITC"
    case BodoniSvtyTwoITCTTBold = "BodoniSvtyTwoITCTT-Bold"
    case BodoniSvtyTwoITCTTBook = "BodoniSvtyTwoITCTT-Book"
    case BodoniSvtyTwoITCTTBookIta = "BodoniSvtyTwoITCTT-BookIta"
    case VerdanaItalic = "Verdana-Italic"
    case VerdanaBoldItalic = "Verdana-BoldItalic"
    case Verdana = "Verdana"
    case VerdanaBold = "Verdana-Bold"
    case AmericanTypewriterCondensedLight = "AmericanTypewriter-CondensedLight"
    case AmericanTypewriter = "AmericanTypewriter"
    case AmericanTypewriterCondensedBold = "AmericanTypewriter-CondensedBold"
    case AmericanTypewriterLight = "AmericanTypewriter-Light"
    case AmericanTypewriterSemibold = "AmericanTypewriter-Semibold"
    case AmericanTypewriterBold = "AmericanTypewriter-Bold"
    case AmericanTypewriterCondensed = "AmericanTypewriter-Condensed"
    case AvenirNextUltraLight = "AvenirNext-UltraLight"
    case AvenirNextUltraLightItalic = "AvenirNext-UltraLightItalic"
    case AvenirNextBold = "AvenirNext-Bold"
    case AvenirNextBoldItalic = "AvenirNext-BoldItalic"
    case AvenirNextDemiBold = "AvenirNext-DemiBold"
    case AvenirNextDemiBoldItalic = "AvenirNext-DemiBoldItalic"
    case AvenirNextMedium = "AvenirNext-Medium"
    case AvenirNextHeavyItalic = "AvenirNext-HeavyItalic"
    case AvenirNextHeavy = "AvenirNext-Heavy"
    case AvenirNextItalic = "AvenirNext-Italic"
    case AvenirNextRegular = "AvenirNext-Regular"
    case AvenirNextMediumItalic = "AvenirNext-MediumItalic"
    case BaskervilleItalic = "Baskerville-Italic"
    case BaskervilleSemiBold = "Baskerville-SemiBold"
    case BaskervilleBoldItalic = "Baskerville-BoldItalic"
    case BaskervilleSemiBoldItalic = "Baskerville-SemiBoldItalic"
    case BaskervilleBold = "Baskerville-Bold"
    case Baskerville = "Baskerville"
    case KhmerSangamMN = "KhmerSangamMN"
    case DidotItalic = "Didot-Italic"
    case DidotBold = "Didot-Bold"
    case Didot = "Didot"
    case SavoyeLetPlain = "SavoyeLetPlain"
    case BodoniOrnamentsITCTT = "BodoniOrnamentsITCTT"
    case Symbol = "Symbol"
    case MenloItalic = "Menlo-Italic"
    case MenloBold = "Menlo-Bold"
    case MenloRegular = "Menlo-Regular"
    case MenloBoldItalic = "Menlo-BoldItalic"
    case BodoniSvtyTwoSCITCTTBook = "BodoniSvtyTwoSCITCTT-Book"
    case Papyrus = "Papyrus"
    case PapyrusCondensed = "Papyrus-Condensed"
    case HiraginoSansW3 = "HiraginoSans-W3"
    case HiraginoSansW6 = "HiraginoSans-W6"
    case PingFangSCUltralight = "PingFangSC-Ultralight"
    case PingFangSCRegular = "PingFangSC-Regular"
    case PingFangSCSemibold = "PingFangSC-Semibold"
    case PingFangSCThin = "PingFangSC-Thin"
    case PingFangSCLight = "PingFangSC-Light"
    case PingFangSCMedium = "PingFangSC-Medium"
    case MyanmarSangamMNBold = "MyanmarSangamMN-Bold"
    case MyanmarSangamMN = "MyanmarSangamMN"
    case EuphemiaUCASItalic = "EuphemiaUCAS-Italic"
    case EuphemiaUCAS = "EuphemiaUCAS"
    case EuphemiaUCASBold = "EuphemiaUCAS-Bold"
    case Zapfino = "Zapfino"
    case BodoniSvtyTwoOSITCTTBook = "BodoniSvtyTwoOSITCTT-Book"
    case BodoniSvtyTwoOSITCTTBold = "BodoniSvtyTwoOSITCTT-Bold"
    case BodoniSvtyTwoOSITCTTBookIt = "BodoniSvtyTwoOSITCTT-BookIt"
    
}
//brandpalettes.com

struct BrandPalettes {
    static let AandWRootBeerBrown = UIColor(red: 79/255, green: 36/255, blue: 0, alpha: 1)
    static let AandWRootBeerOrange =  UIColor(red: 241/255, green: 93/255, blue: 34/255, alpha: 1)
    static let AandWRootBeerTan = UIColor(red: 241/255, green: 93/255, blue: 34/255, alpha: 1)
    static let AcerGreen = UIColor(red: 131/255, green: 184/255, blue: 26/255, alpha: 1)
    static let Aeropostale = UIColor(red: 0, green: 45/255, blue: 98/255, alpha: 1)
    static let AirBandBCoral = UIColor(red: 1, green: 88/255, blue: 93/255, alpha: 1)
    static let AldiBlue = UIColor(red: 0/255, green: 31/255, blue: 120/255, alpha: 1)
    static let AldiLightBlue = UIColor(red: 0/255, green: 181/255, blue: 219/255, alpha: 1)
    static let AldiLightOrange = UIColor(red: 250/255, green: 110/255, blue: 10/255, alpha: 1)
    static let AldiYellow = UIColor(red: 255/255, green: 191/255, blue: 69/255, alpha: 1)
    static let AmazonOrangeYellow = UIColor(red: 255/255, green: 153/255, blue: 0/255, alpha: 1)
    static let AndroidGreen = UIColor(red: 108/255, green: 194/255, blue: 174/255, alpha: 1)
    static let AppleSilver = UIColor(red: 163/255, green: 170/255, blue: 174/255, alpha: 1)
    static let AppleBlack = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
    static let ArbysRed = UIColor(red: 197/255, green: 0/255, blue: 12/255, alpha: 1)
    static let ArthurRed = UIColor(red: 253/255, green: 24/255, blue: 16/255, alpha: 1)
    static let ArthurNavy = UIColor(red: 32/255, green: 78/255, blue: 156/255, alpha: 1)
    static let ArthurYellow = UIColor(red: 238/255, green: 233/255, blue: 59/255, alpha: 1)
    static let ArthurBlue = UIColor(red: 78/255, green: 141/255, blue: 195/255, alpha: 1)
    static let ArthurLightOrange = UIColor(red: 245/255, green: 179/255, blue: 36/255, alpha: 1)
    static let AsusBlue = UIColor(red: 0/255, green: 83/255, blue: 155/255, alpha: 1)
    
    static let BarnesAndNobleGreen = UIColor(red: 54/255, green: 98/255, blue: 81/255, alpha: 1)
    static let BarnesAndNobleTan = UIColor(red: 198/255, green: 190/255, blue: 76/255, alpha: 1)
    
    static let BarneyPurple = UIColor.fromIntRGB(182,38,132)
    static let BarneyGreen = UIColor.fromIntRGB(0,153,73)
    static let BarneyBlue = UIColor.fromIntRGB(56,93,174)
    
    /*
     static let BarqsRootBerrSilver = UIColor.fromRGB(149,165,175)
     (23,149,71)
     (192,42,54)
     (0,0,0)
     
     //blues clues
     (0,174,239)
     (25,79,144)
     
     //bob the builder
     (255,202,2)
     (7,0,69)
     (2,76,161)
     (217,30,38)
     
     //bath and body works
     (0,86,153)
     
     // best buy
     (10,74,191)
     (246,235,22)
     (0.0.0)
     
     //blue moon
     (29,36,92)
     (121,193,241)
     (253,239,226)
     
     // bubble guppies
     (14,133,201)
     (23,189,224)
     (80,189,134)
     
     // buckle
     (139,35,50)
     (0,0,0)
     
     // bud light
     (55,56,142)
     (43,149,225)
     
     // budweiser
     (200,16,46)
     (255,255,255)
     
     // b3w
     (255,210,0)
     (170,170,169)
     (0,0,0)
     (255,255,255)
     
     //bumble
     (255,203,55)
     (255,255,255)
     // burger kking
     (238,29,35)
     (24,84,148)
     (250,175,24)
     white
     
     //busch light
     
     (32,76,133)
     (0,164,218)
     (194,195,197)
     white
     
     //canon
     (191,25,32)
     white
     
     //caribou coffee
     (121,175,188)
     black
     
     // century link
     (17,135,69)
     (141,198,67)
     black
     
     //charter spectrum
     (119,120,123)
     (0,118,188)
     
     //chick fil et
     (229,22,54)
     white
     
     //chipolte
     (168,22,18)
     (69,20,0)
     white
     
     //cocoa cola
     (244,0,9)
     black
     white
     
     //comcast
     (200,0,29)
     black
     
     //coors light
     (200,0,29)
     (117,115,115)
     (203,200,199)
     white
     
     //costco
     (227,24,55)
     (0,93,170)
     white
     
     // culvers
     (0,85,153)
     white
     
     // curious george
     (235,51,35)
     (255,253,84)
     (158,74,27)
     (247,205,175)
     black
     
     //cvs
     (204,0,0)
     white
     
     //
     //dairy queen
     
     (238,62,66)
     (0,122,193)
     (249,170,83)
     white
     
     //ebay
     (249,170,83)
     (0,100,210)
     (245,175,2)
     (134,184,23)
     
     //evian
     (225,25,74)
     (0,124,187)
     (247,216,223)
     white
     
     //faceboo
     (60,90,153)
     white
     
     //fb messenger
     (0,120,255)
     (0,198,255)
     white
     
     //fanta
     (255,131,0)
     (17,36,111)
     (54,150,56)
     (33,102,47)
     white
     
     //ferrari
     (255 242 0)
     black
     (0,140,69)
     white
     (205,33,42)
     
     //five guys
     (201,32,39)
     white
     
     //foot locker
     (238,41,61)
     black
     white
     
     // frontier
     (219,32,44)
     (79,76,77)
     
     //gap
     (0,42,95)
     white
     
     //gatorade
     black
     white
     (229,26,35)
     (244,122,55)
     (0,105,63)
     
     //google analytics
     (0,105,63)
     (245,126,2)
     (255,197,23)
     (117,117,117)
     
     //google
     (66,133,244)
     (219,68,55)
     (244,160,0)
     (15,157,88)
     
     //gopro
     (15,157,88)
     (16,157,217)
     (232,232,234)
     black
     
     //h and m
     (204,7,30)
     white
     
     //HARDEES
     (241,12,38)
     (254,200,48)
     BLACK
     
     //harley DAVIDSON
     (255,103,31)
     (39,37,31)
     
     //HEINEKIN
     (0,130,0)
     (32,85,39)
     (255,43,0)
     (195,195,195)
     
     //HEWLETT PACKARD
     (0,150,214)
     WHITE
     
     // HEY ARNOLD
     (255,210,39)
     (255,101,0)
     BLACK
     
     //hughes
     (0,93,172)
     white
     
     //home depot
     (250, 99, 4)
     
     //hulu
     (102,170,50)
     (102,102,102)
     
     //hy vee
     (226,35,26)
     white
     
     // IKEN
     (0 81 186)
     (255 218 26)
     
     //IN N OUT BURGER
     (198,0,29)
     (255,239,0)
     
     //instgram
     (64,93,230)
     (88,81,219)
     (131,58,180)
     (225,48,108)
     253,29,29)
     (245,96,64)
     (247,119,55)
     (255,220,128)
     
     //JACK IN THE BOX
     WHITE
     (228,1,26)
     (181,10,55)
     
     
     // JC PENNY
     (217,25,32)
     WHITE
     
     //JERSY MIKES
     (238,50,39)
     (19,74,124)
     WHITE
     
     //JIMMY HOHNS
     (206,31,47)
     BLACK
     WHITE
     
     //JOHN DEERE
     (54,124,43)
     (255,222,0)
     (39,37,31)
     
     //KFC
     BLACK
     WHITE
     (163,8,12)
     (245,212,183)
     (255,241,226)
     
     //keystone
     (0,81,137)
     (67,180,227)
     white
     
     //kohl
     (128,0,51)
     white
     
     //kroger
     (4,104,179)
     white
     
     //lacoste
     black
     white
     (4,104,179)
     (219,0,38)
     
     //leggo
     black, white
     (227,0,11)
     (255,237,0)
     
     //lenovo
     (225,20,10)
     white
     
     // Levis
     (196, 18, 48)
     (0,0,0,100)
     white
     
     //LG
     white
     (165,0,52)
     (107,107,107)
     
     
     //Linked in
     white
     (0,119,181)
     black
     
     //little ceasers
     black
     white
     (255,103,29)
     
     //looney
     (255,103,29)
     (216,41,47)
     black
     
     //Maceys
     (231,0,0)
     black
     
     //maurices
     (91,183,178)
     white
     
     //mcdonalds
     (255,199,44)
     (218,41,28)
     
     //micelobe light
     (0,52,109)
     (235,60,72)
     (167,37,64)
     white
     
     // Mickey mouse clubhouse
     
     white
     (238,44,36)
     (44,170,226)
     (84,185,72)
     (249,191,25)
     
     //microsoft
     (242,80,34)
     (127,186,0)
     (0,164,239)
     (255,185,0)
     
     //miller high light
     (169,72,61)
     (158,137,92)
     
     //mooutain due
     black
     white
     (237,26,55)
     (7,112,60)
     (148,201,61)
     
     //mug
     (90,52,31)
     (218,33,39)
     (243,189,55)
     white
     
     // my little pony
     (243,189,55)
     white
     (130,43,153)
     (236,5,142)ite
     
     (241,64,169)
     (55,176,201)
     
     //natural light
     white
     (0,118,187)
     (0,147,212)
     (238,64,55)
     (193,195,197)
     
     //netflix
     (229,9,20)
     black
     bhite
     
     //nike
     black
     white
     
     //nikon
     black
     (255,255,0)
     
     
     //nintendo
     (230,0,18)
     white
     
     //nuvera
     (218,26,50)
     (21,190,240)
     (211,223,78)
     black
     
     // old navy
     (22,62,105)
     white
     
     //pabst blue ribbon
     white
     (16,44,115)
     (197,9,31)
     (196,196,196)
     
     //panda express
     (255,0,0)
     white
     black
     
     //pandora
     (0,160,238)
     white
     
     //panera
     (255,219,135)
     (60,28,0)
     
     //papa johns
     (218,16,46)
     (0,142,106)
     white
     
     //papa murphys
     (212,0,38)
     (0,102,62)
     (226,177,31)
     white
     
     //paw patrol
     (189,34,31)
     (9,158,218)
     (254,227,1)
     (171,183,189)
     white
     
     //[paypal
     (0 69 124)
     (0 121 193)
     black
     white
     
     //pepsie
     white
     (235,25,51)
     (33,81,161)
     
     //periscope
     white
     (64,164,196)
     (233,79,60)
     
     
     //pibb extra
     black
     white
     (246,235,20)
     204,204,204)
     
     // Pinterest
     (189,8,28)
     white
     
     
     //pizzzhut
     (238,58,67)
     white
     
     //playstation 4
     white
     black
     (0 48 135)
     
     //pokemon
     
     (255,203,5)
     (61,125,202)
     (0,58,112)
     
     //popeyes louisiana kitchen
     white
     (196,18,48)
     (245,132,39)
     
     //powerade
     black
     white
     (16,199,221)
     
     //powerpuff girls
     black
     white
     (246,173,205)
     (255,242,0)
     
     //Qdoba
     (249,157,28)
     black
     
     // RC Cola
     (0,83,154)
     (237,50,36)
     white
     (166,168,171)
     
     //red bull
     (204,31,75)
     (0,76,109)
     
     (255,202,6)
     (34,57,114)
     (245,129,32)
     
     (70,70,70)
     (0,140,209)
     (33,43,48)
     (212,214,217)
     (19,32,70)
     (294,32,39)
     
     (245,245,245)
     (72,66,65,79)
     
     //reddit
     black
     white
     (255,69,0)
     
     //safeway
     black
     white
     (214,35,42)
     
     //sam's club
     white
     RGB: (0,75,141)
     CMYK: (100,79,16,3)
     
     GREEN
     PANTONE: PMS 7737 C (NEAREST MATCH)
     HEX COLOR: #5D9732;
     RGB: (93,151,50)
     CMYK: (69,20,100,4)
     MEDIUM BLUE
     PANTONE: PMS 7691 C (NEAREST MATCH)
     HEX COLOR: #0069AA;
     RGB: (0,105,170)
     CMYK: (92,58,7,1)
     
     LIGHT BLUE
     PANTONE: PMS 279 C (NEAREST MATCH)
     HEX COLOR: #0081C6;
     RGB: (0,129,198)
     
     
     //samsung
     BLUE
     PANTONE: PMS 293 C (NEAREST MATCH)
     HEX COLOR: #034EA2;
     RGB: (3,78,162)
     CMYK: (99,78,2,0)
     
     WHITE
     
     //samuel adams
     NAVY BLUE
     PANTONE: PMS 655 C (NEAREST MATCH)
     HEX COLOR: #002856;
     RGB: (0,40,86)
     CMYK: (100,89,36,35)
     
     RED
     PANTONE: PMS 185 C (NEAREST MATCH)
     HEX COLOR: #EB0029;
     RGB: (235,0,41)
     
     //sesame street
     white
     GREEN
     PANTONE: PMS 361 C (NEAREST MATCH)
     HEX COLOR: #00AF42;
     RGB: (0,175,66)
     CMYK: (81,1,100,0)
     
     YELLOW
     PANTONE: PMS 116 C (NEAREST MATCH)
     HEX COLOR: #FCD000;
     RGB: (252,208,0)
     CMYK:
     
     // shake shack
     GREEN
     PANTONE: PMS 360 C (NEAREST MATCH)
     HEX COLOR: #6CB33F;
     RGB: (108,179,63)
     CMYK: (64,6,100,0)
     
     BLACK
     
     //sierra mist
     GREEN
     PANTONE: PMS 7739 C (NEAREST MATCH)
     HEX COLOR: #2CA544;
     RGB: (44,165,68)
     CMYK: (80,8,100,1)
     
     LIME GREEN
     PANTONE: PMS 367 C (NEAREST MATCH)
     HEX COLOR: #89C444;
     RGB: (137,196,68)
     CMYK: (51,1,97,0)
     
     YELLOW
     PANTONE: PMS 107 C (NEAREST MATCH)
     HEX COLOR: #FFDE05;
     RGB: (255,222,5)
     
     WHITE
     
     //skype
     BLUE
     PANTONE: PMS 299 C (NEAREST MATCH)
     HEX COLOR: #00AFF0;
     RGB: (0,175,240)
     
     white
     
     //slack
     BLUE
     PANTONE: PMS 298 C
     HEX COLOR: #36C5F0;
     RGB: (54 197 240)
     CMYK: (65 10 2 0)
     
     YELLOW
     PANTONE: PMS 7409 C
     HEX COLOR: #ECB22E;
     RGB: (236 178 46)
     CMYK: (4 33 98 0)
     
     GREEN
     PANTONE: PMS 2250 C
     HEX COLOR: #2EB67D;
     RGB: (46 182 125)
     CMYK: (79 2 75 0)
     
     RED
     PANTONE: PMS 214 C
     HEX COLOR: #E01E5A;
     RGB: (224 30 90)
     CMYK: (13 100 36 0)
     
     AUBERGINE
     PANTONE: PMS 261 C
     HEX COLOR: #4A154B;
     RGB: (74 21 75)
     CMYK: (68 95 33 22)
     
     BLACK
     
     
     //snapchat
     black
     white
     YELLOW
     PANTONE: PMS 3945 C (NEAREST MATCH)
     HEX COLOR: #FFFC00;
     RGB: (255,252,0)
     
     // sonic
     RED
     PANTONE: PMS RED 032 C (NEAREST MATCH)
     HEX COLOR: #EF3B44;
     RGB: (239,59,68)
     CMYK: (0,91,74,0)
     
     BLUE
     PANTONE: PMS 7684 C (NEAREST MATCH)
     HEX COLOR: #0A68B0;
     RGB: (10,104,176)
     CMYK: (91,59,2,0)
     
     YELLOW
     PANTONE: PMS 107 C (NEAREST MATCH)
     HEX COLOR: #FCDD2A;
     RGB: (252,221,42)
     CMYK: (3,9,93,0)
     
     LIGHT PURPLE
     PANTONE: PMS 7681 C (NEAREST MATCH)
     HEX COLOR: #97A8D3;
     RGB: (151,168,211)
     
     
     // sound cloud
     
     ORANGE
     PANTONE: PMS ORANGE 021 C
     HEX COLOR: #FE5000;
     RGB: (254 80 0)
     
     // sponge bob
     
     YELLOW
     PANTONE: PMS 100 C (NEAREST MATCH)
     HEX COLOR: #FFF56C;
     RGB: (247,235,98)
     CMYK: (6,1,75,0)
     
     DARK YELLOW
     PANTONE: PMS 7765 C (NEAREST MATCH)
     HEX COLOR: #AEAD0D;
     RGB: (187,179,34)
     CMYK: (31,21,100,1)
     
     AQUA BLUE
     PANTONE: PMS 631 C (NEAREST MATCH)
     HEX COLOR: #26B9C8;
     RGB: (55,176,201)
     CMYK: (69,10,18,0)
     
     BLUE
     PANTONE: PMS 7686 C (NEAREST MATCH)
     HEX COLOR: #0457A0;
     RGB: (25,79,144)
     CMYK: (98,77,14,2)
     
     WHITE
     
     //spotify
     GREEN
     PANTONE: PMS 2257 C (NEAREST MATCH)
     HEX COLOR: #1DB954;
     RGB: (30,215,96)
     CMYK: (80,0,80,0)
     
     WHITE
     HEX COLOR: #FFFFFF;
     RGB: (255,255,255)
     CMYK: (0,0,0,0)
     
     BLACK
     HEX COLOR: #000000;
     RGB: (25,20,20)
     
     
     //
     //sprint
     YELLOW
     PANTONE: PMS 107 C (NEAREST MATCH)
     HEX COLOR: #FFDD05;
     RGB: (255,221,5)
     CMYK: (2,9,99,0)
     
     BLACK
     
     //sprite
     BLUE
     PANTONE: PMS 7455 C (NEAREST MATCH)
     HEX COLOR: #1E5CB3;
     RGB: (30,92,179)
     CMYK: (90,69,0,0)
     
     YELLOW
     PANTONE: PMS 3945 C (NEAREST MATCH)
     HEX COLOR: #FFF100;
     RGB: (255,241,0)
     CMYK: (4,0,94,0)
     
     GREEN
     PANTONE: PMS 7739 C (NEAREST MATCH)
     HEX COLOR: #00A65F;
     RGB: (0,166,95)
     CMYK: (82,7,85,1)
     
     WHITE
     
     //
     //starbucks
     GREEN
     PANTONE: PMS 7728 C (NEAREST MATCH)
     HEX COLOR: #067655;
     RGB: (6,118,85)
     CMYK: (88,30,78,17)
     
     BLACK
     WHITE
     
     // subway
     GREEN
     PANTONE: PMS 343 C (NEAREST MATCH)
     HEX COLOR: #005542;
     RGB: (0,85,66)
     CMYK: (90,41,76,38)
     
     YELLOW
     PANTONE: PMS 3945 C (NEAREST MATCH)
     HEX COLOR: #FFF200;
     RGB: (255,242,0)
     CMYK: (4,0,93,0)
     
     WHITE
     
     //sun drop
     
     //RED
     PANTONE: PMS 485 C (NEAREST MATCH)
     HEX COLOR: #D71921;
     RGB: (215,25,33)
     CMYK: (9,100,100,2)
     
     YELLOW
     PANTONE: PMS 3945 C (NEAREST MATCH)
     HEX COLOR: #FFF219;
     RGB: (255,242,25)
     CMYK: (4,0,91,0)
     
     GREEN
     PANTONE: PMS 7739 C (NEAREST MATCH)
     HEX COLOR: #22B24C;
     RGB: (34,178,76)
     CMYK: (78,1,99,0)
     
     WHITE
     
     //sunkist
     LUE
     PANTONE: PMS 661 C (NEAREST MATCH)
     HEX COLOR: #0E2B89;
     RGB: (14,43,137)
     CMYK: (100,94,14,5)
     
     ORANGE
     PANTONE: PMS 1575 C (NEAREST MATCH)
     HEX COLOR: #FE7D1A;
     RGB: (254,125,26)
     CMYK: (0,63,99,O)
     
     GREEN
     PANTONE: PMS 7731 C (NEAREST MATCH)
     HEX COLOR: #018240;
     RGB: (1,130,64)
     CMYK: (88,24,100,11)
     
     LIGHT ORANGE
     PANTONE: PMS 1375 C (NEAREST MATCH)
     HEX COLOR: #FF9E0D;
     RGB: (255,158,13)
     CMYK: (0,45,100,0)
     
     WHITE
     
     //superman
     RED
     PANTONE: 186 C
     HEX COLOR: #C80000;
     RGB: (200,0,0)
     CMYK: (15,100,100,5)
     
     YELLOW
     PANTONE: 3945 C
     HEX COLOR: #FFEB00;
     RGB: (255,235,0)
     CMYK: (3,2,98,0)
     
     BLACK
     
     //SURGE
     RED
     PANTONE: PMS 485 C (NEAREST MATCH)
     HEX COLOR: #ED1C24;
     RGB: (237,28,36)
     CMYK: (1,99,97,0)
     
     LIME GREEN
     PANTONE: PMS 2299 C (NEAREST MATCH)
     HEX COLOR: #B2D234;
     RGB: (178,210,52)
     CMYK: (35,0,99,0)
     
     BLACK
     
     //T-MOBILE
     PINK
     PANTONE: PMS 219 C (NEAREST MATCH)
     HEX COLOR: #ED008C;
     RGB: (237,0,140)
     CMYK: (0,99,1,0)
     
     GRAY
     PANTONE: PMS COOL GRAY 7 C (NEAREST MATCH)
     HEX COLOR: #999B9E;
     RGB: (153,155,158)
     
     
     // TACO BELL
     PURPLE
     PANTONE: PMS 526 C (NEAREST MATCH)
     HEX COLOR: #682A8D;
     RGB: (104,42,141)
     CMYK: (74,100,3,1)
     
     LIGHT PURPLE
     PANTONE: PMS 521 C (NEAREST MATCH)
     HEX COLOR: #A07EBA;
     RGB: (160,126,186)
     CMYK: (39,55,0,0)
     
     BLACK
     
     //TARGET
     RED
     PANTONE: PMS 2035 C (NEAREST MATCH)
     HEX COLOR: #CC0000;
     RGB: (204,0,0)
     CMYK: (13,100,100,4)
     
     WHITE
     
     // TEENAGE MUTANT NINJA TURGLE
     RED
     PANTONE: PMS 485 C (NEAREST MATCH)
     HEX COLOR: #ED1C24;
     RGB: (225,38,28)
     CMYK: (6,97,100,1)
     
     GREEN
     PANTONE: PMS 375 C (NEAREST MATCH)
     HEX COLOR: #8FD129;
     RGB: (148,214,0)
     CMYK: (47,0,100,0)
     
     BLACK
     WHITE
     
     //TIM HORTONS
     RED
     PANTONE: PMS 199 C (NEAREST MATCH)
     HEX COLOR: #DD0F2D;
     RGB: (221,15,45)
     CMYK: (7,100,91,1)
     
     WHITE
     
     //TIM HORTONS
     RED
     PANTONE: PMS 199 C (NEAREST MATCH)
     HEX COLOR: #DD0F2D;
     RGB: (221,15,45)
     CMYK: (7,100,91,1)
     
     WHITE
     
     
     // TINDER
     PINK
     PANTONE: PMS 191 C (NEAREST MATCH)
     HEX COLOR: #FE3C72;
     RGB: (254,60,114)
     CMYK: (0,89,33,0)
     
     GRAY
     PANTONE: PMS 446 C (NEAREST MATCH)
     HEX COLOR: #424242;
     RGB: (66,66,66)
     
     //TRADER JOE
     RED
     PANTONE: PMS 1935 C (NEAREST MATCH)
     HEX COLOR: #TD21242;
     RGB: (210,18,66)
     CMYK: (12,100,73,2)
     
     WHITE
     
     // TWITCH
     PURPLE
     PANTONE: PMS 2665 C
     HEX COLOR: #6441A4;
     RGB: (100,65,164)
     CMYK: (69,75,0,0)
     
     WHITE
     
     // TWITTER
     BLUE
     PANTONE: 2382 C
     HEX COLOR: #1DA1F2;
     RGB: (29,161,242)
     CMYK: (69,26,0,0)
     
     Twitter Secondary Color Palette
     BLACK
     PANTONE: BLACK 7 C
     HEX COLOR: #14171A;
     RGB: (20,23,26)
     CMYK: (76,68,63,78)
     
     DARK GRAY
     PANTONE: COOL GRAY 9 C
     HEX COLOR: #657786;
     RGB: (101,119,134)
     CMYK: (65,46,37,8)
     
     LIGHT GRAY
     PANTONE: COOL GRAY 7 C
     HEX COLOR: #AAB8C2;
     RGB: (170,184,194
     CMYK: (34,20,18,0)
     
     EXTRA LIGHT GRAY
     PANTONE: CPP; GRAY 3 C
     HEX COLOR: #E1E8ED;
     RGB: (225,232,237)
     CMYK: (10,4,4,0)
     
     EXTRA EXTRA LIGHT GRAY
     PANTONE: COOL GRAY 1 C
     HEX COLOR: #F5F8FA;
     RGB: (245,248,250)
     CMYK: (3,1,1,0)
     
     WHITE
     
     //VERIXON
     RED
     PANTONE: PMS 2035 C (NEAREST MATCH)
     HEX COLOR: #CD040B;
     RGB: (205,4,11)
     CMYK: (13,100,100,4)
     
     BLACK
     
     //VIBER
     VIBER PURPLE
     HEX COLOR: #665CAC;
     RGB: (102, 92, 172)
     CMYK: (69,72,0,0)
     
     VIBER BLUE
     HEX COLOR: #54C0D4;
     RGB: (84,192,212)
     CMYK: (61,3,15,0)
     
     VIBER RED
     HEX COLOR: #EF6062;
     RGB: (239, 96, 98)
     CMYK: (0,7,55,0)
     
     VIBER YELLOW
     HEX COLOR: #F4EF7B;
     RGB: (237,236,130)
     CMYK: (9,0,62,0)
     
     DARK GREY
     HEX COLOR: #4A4A4A;
     RGB: (74 74 74)
     CMYK: (66,59,57,39)
     
     GREY
     HEX COLOR: #B0B0B0;
     RGB: (176 176 176)
     CMYK: (32 25 26 0)
     
     WHITE
     
     //WALGREENS
     RED
     PANTONE: PMS 199 C (NEAREST MATCH)
     HEX COLOR: #E31836;
     RGB: (227,24,54)
     CMYK: (5,100,84,1)
     
     WHITE
     
     //WALMART
     MEDIUM BLUE
     PANTONE: 285 C
     HEX COLOR: #0072CE;
     RGB: (0,114,206)
     CMYK: (90,48,0,0)
     
     YELLOW
     PANTONE: 1235 C
     HEX COLOR: #FFB81C;
     RGB: (253,187,48)
     CMYK: (0,29,91,0)
     
     Walmart Secondary Color Palette
     DARK BLUE
     PANTONE: 287 C
     HEX COLOR: #003087;
     RGB: (0,56,150)
     CMYK: (100,68,0,12)
     
     LIGHT BLUE
     PANTONE: 284 C
     HEX COLOR: #6CACE4;
     RGB: (108,171,231)
     CMYK: (55,19,0,0)
     
     //WENDYS
     RED
     PANTONE: PMS 485 C (NEAREST MATCH)
     HEX COLOR: #ED1B24;
     RGB: (237,27,36)
     CMYK: (1,99,97,0)
     
     YELLOW
     PANTONE: PMS 3945 C (NEAREST MATCH)
     HEX COLOR: #FAF600;
     RGB: (250,246,0)
     CMYK: (7,0,94,0)
     
     LIGHT BLUE
     PANTONE: PMS 636 C (NEAREST MATCH)
     HEX COLOR: #8ED5E6;
     RGB: (142,213,230)
     CMYK: (41,0,8,0)
     
     BLACK
     WHITE
     
     //WHATABURGER
     ORANGE
     PANTONE: PMS 1585 C (NEAREST MATCH)
     HEX COLOR: #FF770F;
     RGB: (255,119,15)
     CMYK: (0,66,100,0)
     
     WHITE
     
     //WHATS APP
     GREEN
     PANTONE: PMS 360 C (NEAREST MATCH)
     HEX COLOR: #4AC959;
     RGB: (74,201,89)
     CMYK: (66,0,87,0)
     
     GRAY
     PANTONE: PMS 7545 C (NEAREST MATCH)
     HEX COLOR: #455A64;
     RGB: (69,90,100)
     CMYK: (75,55,47,24)
     
     WHITE
     
     //WHITE CASTLE
     BLUE
     PANTONE: PMS 2745 C (NEAREST MATCH)
     HEX COLOR: #011375;
     RGB: (1,19,117)
     CMYK: (100,97,21,19)
     
     ORANGE
     PANTONE: PMS 137 C (NEAREST MATCH)
     HEX COLOR: #FBA301;
     RGB: (251,163,1)
     CMYK: (0,41,100,0)
     
     WHITE
     
     //WHOLE FOODS
     GREEN
     PANTONE: PMS 342 C (NEAREST MATCH)
     HEX COLOR: #00674B;
     RGB: (0,103,75)
     CMYK: (90,35,78,26)
     
     WHITE
     
     //WINDSSTREAM
     
     GREEN
     PANTONE: PMS 368 C (NEAREST MATCH)
     HEX COLOR: #7FCC28;
     RGB: (127,204,40)
     CMYK: (54,0,100,0)
     
     BLACK
     
     //XBOX
     GREEN
     PANTONE: PMS 2259 C (NEAREST MATCH)
     HEX COLOR: #107C10;
     RGB: (16,124,16)
     CMYK: (87,26,100,15)
     
     BLACK
     
     //YOUTUBE
     RGB: (255,0,0)
     CMYK: (0,95,100,0)
     PANTONE: PMS 2347 C (NEAREST MATCH)
     
     ALMOST BLACK
     HEX COLOR: #282828;
     RGB: (40,40,40)
     
     // ZAPPOS
     
     ZAPPOS BLUE
     PANTONE: PMS 285 C (NEAREST MATCH)
     HEX COLOR: #0076BD;
     RGB: (1,118,189)
     CMYK: (87,49,0,0)
     
     WHITE
     BLACK
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     */
}

#warning("add brand colors, and add font names (from downloaded csv ")
// or create a structure for each brand containing its pallete of coolors


extension UIColor {
    static func fromIntRGB(_ r:Int, _ g:Int, _ b:Int) -> UIColor {
        return UIColor(red: CGFloat(r/255) , green: CGFloat(g/255) , blue: CGFloat( b/255) , alpha: 1)
    }
}

