//
//  models.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

/*
class Book {
    let title:String,
        author:String,
        image:UIImage,
        pages:[Page]
    
    init(title:String, author:String, image:UIImage, pages:[Page]){
        self.title = title
        self.author = author
        self.image = image
        self.pages = pages
    }
}

class Page {
    
    let number:Int, text:String
    
    init (number:Int, text:String ) {
        self.number = number
        self.text = text
    }
}
 */

struct Book:Decodable {
    let id:Int,
        title, author, coverImageUrl:String
    var pages:[Page]?
    
}
struct Page: Decodable {
    let id:Int, text:String
    
}
    
   typealias Kindle = [Book]

