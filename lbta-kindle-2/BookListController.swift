//
//  ViewController.swift
//  lbta-kindle-2
//
//  Created by Admin on 8/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class BookListController: UITableViewController {

    var arrBook:[Book]?
    
    let urlText = "https://letsbuildthatapp-videos.s3-us-west-2.amazonaws.com/kindle.json"

    override func viewDidLoad() {
        super.viewDidLoad()
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        setup()
        //setupBooks()
        fetchBooks()
    }

    func setup() {
        
        navigationItem.title = "Kindle App"
        tableView.register(CustomCell.self, forCellReuseIdentifier: "A" )
        tableView.tableFooterView = UIView()
        tableView.backgroundColor  = axTheme.color(of: .tableViewBackground)
    }
 
    
    
    func fetchBooks() {
        
        guard let url = URL(string: urlText) else {
            fatalError()
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("There is an error ", error ?? "" )
            }
            guard let data = data else {
                fatalError("no data")
            }
            
           let s = String(bytes: data, encoding: .utf8)
            
            print(s ?? "")
            print("-------------------------")
            do {
                let aJSONDecoder = JSONDecoder()
                self.arrBook = try aJSONDecoder.decode(Kindle.self, from: data )
            } catch let jerror {
                print("Decode error")
                print(jerror.localizedDescription)
            }
            
            for (ix,b) in (self.arrBook?.enumerated())! {
                
                print(ix,b.title )
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        task.resume()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBook?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "A", for: indexPath) as! CustomCell
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        cell.bookForCell = arrBook?[indexPath.row]
        
        cell.titleLabel.backgroundColor = axTheme.color(of: .labelBackground)
        cell.titleLabel.textColor = axTheme.color(of: .labelText)
        cell.authorLabel.textColor = axTheme.color(of: .labelText)
        cell.authorLabel.backgroundColor = axTheme.color(of: .labelBackground)
        
      
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        let lo = UICollectionViewFlowLayout()
        let vc = PagerViewController(collectionViewLayout: lo)
        vc.book = arrBook?[indexPath.row]
        let nav = UINavigationController(rootViewController: vc )
        present(nav, animated: true, completion: nil )
    
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
 
        let fView = UIView()
        fView.backgroundColor = axTheme.color(of: .tableViewFooterView)
        
        [gridButton,sourceSegmentedControl,sortButton].forEach {
            fView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints=false
        }
            
        NSLayoutConstraint.activate([
                sourceSegmentedControl.widthAnchor.constraint(equalToConstant: 200),
                sourceSegmentedControl.heightAnchor.constraint(equalToConstant: 40),
                sourceSegmentedControl.centerYAnchor.constraint(equalTo: fView.centerYAnchor),
                sourceSegmentedControl.centerXAnchor.constraint(equalTo: fView.centerXAnchor),
                
                gridButton.centerYAnchor.constraint(equalTo: fView.centerYAnchor),
                gridButton.leftAnchor.constraint(equalTo: fView.leftAnchor, constant:8),
                gridButton.heightAnchor.constraint(equalToConstant: 40),
                gridButton.widthAnchor.constraint(equalToConstant: 40),
                
                sortButton.centerYAnchor.constraint(equalTo: fView.centerYAnchor),
                sortButton.rightAnchor.constraint(equalTo: fView.rightAnchor, constant:-8),
                sortButton.heightAnchor.constraint(equalToConstant: 40),
                sortButton.widthAnchor.constraint(equalToConstant: 40),
                
                
        ])
        
        return fView
    
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50 
    }
    
    lazy var sourceSegmentedControl:UISegmentedControl = {
        let ui = UISegmentedControl(items: ["Cloud", "Device"])
        ui.addTarget(self, action: #selector(sourceSegmentedControlValueChangedSelector), for: .valueChanged) //NOTE IT IS "VALUECHANGED" !!!!
        ui.tintColor = axTheme.color(of: .segmentedControlTint)
        ui.selectedSegmentIndex = 0
        ui.sizeToFit()
        return ui
    }()
    @objc func sourceSegmentedControlValueChangedSelector() {
        switch true {
        case sourceSegmentedControl.selectedSegmentIndex == 0: let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
        case sourceSegmentedControl.selectedSegmentIndex == 1: let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
        default: fatalError()
        }
    }
    
    lazy var gridButton:UIButton = {  //snip zxuibtnl
        let ui = UIButton(type: .system)
        ui.setImage(UIImage(named: "grid"), for: .normal)
        ui.image(for: .normal)?.withRenderingMode(.alwaysOriginal)
        ui.tintColor = axTheme.color(of: .buttonTint)
          // OR ...
        ui.tintColor = UIColor.white
        ui.addTarget(self, action: #selector(gridButtonTouchSelector), for: .touchUpInside)
        return ui
    }()
    @objc fileprivate func gridButtonTouchSelector(sender:Any) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
    }
    
    lazy var sortButton:UIButton = {  //snip zxuibtnl
        let ui = UIButton(type: .system)
        ui.setImage(UIImage(named: "sorting"), for: .normal)
        ui.image(for: .normal)?.withRenderingMode(.alwaysOriginal)
        ui.tintColor = axTheme.color(of: .buttonTint)
        ui.addTarget(self, action: #selector(sortingButtonTouchSelector), for: .touchUpInside)
        return ui
    }()
    @objc fileprivate func sortingButtonTouchSelector(sender:Any) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n\u{2705} \(#function) Line \(#line) of \(file)\n")
    }
    
}


